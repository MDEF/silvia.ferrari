---
title: Emergent Business Models
period: 16-30 May 2019
date: 2019-01-24 12:00:00
term: 3
published: True
---

Emergent Business models was the last class we had in the master, and it covered the last mile we had to cover with our projects: reflecting on the impact value they have to find a way to make them grow in the future.


Having worked in innovation consultancy for such a long time, I feel I am familiar with the topics covered during class. Nonetheless, I liked it very much. The challenge for us was to define the inner value of our projects, as well as a path towards making our idea economically sustainable and impactful from a social perspective in the short to mid period.

The model proposed by **Ideas for Change**, who run the course,  is called Pentagrowth and aims at enabling **exponential growth** for our ideas leveraging on the ecosystem they belong to. I think it's important to highlight that by using the term *growth*, the model doesn't mean only economic growth, value in general, meaning also growth in the impact capacity of our projects.

![]({{site.baseurl}}/pentagrowth.jpg)


Leveraging on the ecosystem is a smart strategy: competitive advantage doesn't make sense in a world where knowledge is globally connected and ideas can be replicated easily all over the world. What makes it possible to create impact is the **ability to connect values and goals** trough:
- Connecting the network
- Collecting assets
- Empowering users
- Enabling other partners
- Sharing the knowledge
into the ecosystem we move in with our ideas through steps that represent the growth process.


I think this perspective makes sense, especially for **Doctors of the Future**, my project which is described more in detail [here](Doctors of the Future).
My idea doesn't bring anything new to the table in terms of technology, it leverages on shared knowledge and technology and combines elements that are already present in the system to create a new solution and a methodology to tackle an emergent issue.

## Define the impact of Doctors of the Future

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vS8JEchD3_kOBj4M5ATq3XTnwcyhDqtIaOKeRTRitrNep-KuQ_9FWcr-gC2IAuSpmf20PRYOz_InQCV/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

The first exercise we did aimed at understanding which variables of the system our projects aim to impact on.

In my case, Doctors of the Future aims at:

- Developing knowledge on systemic interconnection, changing the relationships in healthcare

- Developing a behavioral change in our attitude towards health and wellbeing, changing the rules in healthcare

- Developing new infrastructure to monitor our health, changing the roles and relationships in healthcare


## Define the roadmap to maximize the impact of Doctors of the Future

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQC2LUDh8rpTC-o_qGVXqtewuTY_H5Q1_yrgkQU2K9jNY3ZyqxFGKthmPH7tQz55gdIsXKhK50adkw-/embed?start=false&loop=false&delayms=60000" frameborder="0" width="1122" height="1617" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

The second exercise we did aimed at applying the theory we had learn in the three sessions to our project, envisioning a roadmap to make it impact in the way we defined in the first exercise.

Doctors of the future challenges the dominant regime in the healthcare system.

In the dominant narrative, we  **treat health from the point of view of the single patient without taking into consideration the system he lived in**. Doctors of the future brings a new perspective, by imagining new tools and ways of working for the doctors, creating relationship with local communities and monitoring ecosystems and people’s wellbeing. Doing so, doctors will able to understand healthcare from a systemic perspective

The main effort to deploy Doctors of the Future is to make a behavioural change happen. Introducing a new way to tackle collective health, Doctor of the Future is scalable.Communities can start using but since it frames health as a collective issue, it requires a shift in the mindset of the people presently involved in healthcare and urban planning.

To minimize this effort and maximize its impact, it is necessary to involve stakeholders into the process:

- Communities to start monitoring collective health
- Institutional partners in healthcare and urban planning to develop the process
- Doctors to experiment and improve the idea and the tool together with makers
- Communities, designers, data scientists and policymakers to develop data frameworks that ensure ownership to who produce the data
