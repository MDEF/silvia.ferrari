---
title: Design for the new - Practices
period: 9 May 2019
date: 2019-01-24 12:00:00
term: 3
published: True
---
This week assignment for the Design for the New class is to map the practice behind our final intervention as it is today and as it will be in the future.

A practice is defined as *‘A routinized type of behaviour which
consists of several elements, interconnected to one other: forms of bodily activities, forms of mental activities, ‘things’ and their use, a background knowledge in the form of understanding, know- how, states of emotion and motivational knowledge.’
(Reckwitz 2002a:249)*

A simple visualization of this concept visualize practices as the sum of:

![]({{site.baseurl}}/framework.png)

- Skills: mental and physical routines embedded in our behavior related to the practice
- Stuff: the material elements used in the practice
- Images: symbols that give meaning to the practice or shared ideas and beliefs connected to the practice
- Links: the fleeting connections  between the three elements, which contribute in defining what the practice is made of

First of all we group together following the topics behind our projects, to help us define the practices around our interventions
The exercise that we have to do together is to map the network of **actants** around a general topic that bring our projects together.

We choose **health** as a topic and map the ecosystem:
- In black we define the ecosystem
- In green we map the actors playing into it
- In red we map the ideas and practices behind it

![]({{site.baseurl}}/IMG_3403.jpg)

The exercise is really useful for me, because it help me defining the space that I'm moving into with my intervention.

My idea is called Doctors of the Future, and it is an intervention exploring how the urban context we live in impacts on our health and well-being. The aim of the intervention is to understand and improve our life conditions by exploiting technology, intervening on our context, meaning the connection we share with our ecosystem. More info are available [here](https://mdef.gitlab.io/silvia.ferrari/project/).

Reflecting on the space of my intervention makes it very easy to find out which is the main practice behind it. I realize that I am working with **how we understand how we feel from the perspective of health and wellbeing**.

## Practices of understanding how we feel from the perspective of our health and wellbeing

How is the practice i'm working with at the moment? Which are our behaviours around it?

**Skills:**
- Self tracking
- Seeking medical advice
- Doing sports
- Checking online for medical advice
- Setting goals

Links to images: through my skills I will *fulfill* my images
Links to stuff: Skills are performed through stuff

**Images**
- Being a perfect machine
- Being unique
- Being a closed system where everything should work
- Perfect values
- Medical exams

Links to stuff: there is a perceived right stuff that helps me fulfilling my images

**Stuff**
- Phone
- Apps
- Personal data
- Medicines
- Books
- Internet
- Medical exams
- Practitioners in the field


## Practices of understanding how we feel/our health and wellbeing IN THE FUTURE
What changes with my intervention?
**Doctors of the future** provides a new understanding of our health and wellbeing as an interconnected manifestation. We are affected by the collectivity and the exposures (environmental+social) it creates.

**Skills**
- Systemic view of health and wellbeing
- Collective and personal tracking - in relation to collective data
- Ownership of data & sharing

Links to images: through my skills I will *understand* my images and be able to act on them
Links to stuff: Skills are performed through stuff

**Images**
- We are connected to our environment
- We cannot evaluate our state in isolation
- There is no perfect value to reach indipendently from other people
- The space we live in influences us

Links to stuff: stuff can help me having insights on my images

**Stuff**
- Sensors
- Collective data
- Phone
- Apps
- Collective medicines
- Internet
- Nature
- Artificial intelligence
- Urban Doctors

Overall, the exercise helps me understanding the impact of the work i'm doing, and because of that it makes me realize how it should be realized. I start understanding that the biggest effort for my project to become reality is to create **a shift in the mindset** of urban population, policy-makers, healthcare professionals and urban planners
