---
title: 05. Navigating the Uncertainty
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---
The aim of this week was to frame the discourse on the future and emerging technologies from the perspective of design, reflecting on its role in influencing society and change-making. The key message of the week, for me, it was that **design holds a considerable power in the framing of such issues**, from how they are openly communicated to how design can carry hidden messages about them. I ended the week with a very structured **framework for intervention in mind, one that enables to intervene by rethinking interfaces and using design as a leverage point of change**.

### Social systems

Society is [defined in the Merriam Webster dictionary](https://www.merriam-webster.com/dictionary/society) as “an enduring and cooperating social group whose members have developed organized patterns of relationships through interaction with one another”.

Social systems are based on relationships and interactions. Put simply, systems are based on the exchange of information.  
To exchange something, to be able to interact with something that is other than us, we have to define a boundary between us and them. We have learned in [Biology 0](https://mdef.gitlab.io/silvia.ferrari/reflections/biology-zero/) that **life started when boundaries were created: membranes originated cells because the isolation they provided allowed for the elements of the cell to interact and develop a system that works in itself and is able to communicate with the external environment**.

The same concept, I discovered this week, applies in every scale, smaller and bigger than a cell. Boundaries define us and interactions shape our present and future actions.

### Interfaces as a way to understand systems

Boundaries and interactions are never neutral.
Boundaries are very flexible entities, that change as the object of the interaction or the actors involved change.
Interactions, or the act of exchanging information, is never direct. The exchange is always mediated by an interface, that allows us to understand what is other than us. How and which information is exchanged depends on the interface we are using to do it.
Interfaces are the ultimate tool we use to create and share the stories we tell about ourselves and our world, and they are not neutral.

We have learned during the week how, **by relating concepts and matter through a discourse, design holds power and is able to create and transform society** from objects to intangible items.
Michel Foucault offers an example of this power by analyzing the design of chairs: the shape of a chair tell us about our needs, how we have to behave in a room and the choices we make.
Another example of the power of design is how we structure our educational systems: by designing educational systems we decide which rules, which skills and which behaviours are going to shape the future. By educating citizens we teach social control and we advance a specific idea of society and its future.

Discourses define how we experience our systems. Slavoj Zizek [challenges a common contemporary belief](https://medium.economist.com/are-liberals-and-populists-just-searching-for-a-new-master-9d5a4f0cf0a9) by which the rise of populism and nationalism is explained through the emergence of an anti-establishment movement aimed at the rejection of the centralized power. To him, the central issue in discussing nationalism and populism is the discourse we have on power in our systems. In his view, power in systems belongs to a moral authority. Since 1968, he says, power in western politics has belonged to *the experts*. Expertise is just another interface institutionalized power has used to advance very ideological political stances, such as the EU- Greece agreement in 2014 or the emergence of the “governments of technicians” in Italy since 2011. According to Zizek, populism and nationalism are trying to reframe the discourse about power by proposing a new moral authority to replace “the experts”: a very traditional authoritarian master.

### Interfaces as leverage points

We have learned that boundaries, interfaces and discourses are design black boxes: we don’t need to see them, they are hidden, but we are still influenced by them because we delegate task and responsibilities regarding the way we understand what is around us.

Because of this, **design contributes greatly to the way we understand a system: the way we define a boundary and the information that has to be exchanged, the way we think of interactions and the way we design an interface to communicate them are going to influence the global discourse and all the relationships that will emerge from that interface**.

The design of interfaces could be a powerful tool of change-making. According to Donella Meadows, [to intervene in the dynamics of a system we use leverage points](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/). We have a finite number of possible leverage points, that she lists in order of increasing efficiency: the more it is possible to intervene on the rules, the structure, the goals and the mindset that constitute a system, the more the leverage is strong.  By giving us a way to understand the system, interfaces work exactly with these mechanisms. **The definition of boundaries, information to exchange, the design of interfaces and the shaping of a discourse are the most impactful leverages in a system**.

### Emerging interfaces

Interfaces have the power to create new exchanges of information, to connect us with new systems. Since last week, I have been wondering about all the interfaces we currently use to understand our systems and all the ones we still have to design to understand things differently.
**The more we are able to understand our environment through interfaces, to interact with what surrounds us, the more we understand ourselves as part of bigger and complex ecosystems.**

The invention of the microscope was a very illuminating example to me: what was before hidden to us was suddenly there, through a powerful material interface, and we have since been able to relate to a whole new level to nature. Our way of understanding ourselves has changed so much that we now know how tight is the relationship between humans and bacterias in the same environment.

### Impact on my vision

The whole week has been really useful in terms of impact on my vision for two reasons:

* **It helped me defining a framework for my intervention:** With the help of other classmates I made a mind map to organize my thoughts. The exercise was incredibly useful, as it allowed me to understand that I want to work within the gap that always exists between the governance of a system (a city in my case) and the collective community that participate in it (the citizens). **New technologies hold a democratizing power, and I want to exploit it to investigate new emerging interactions in this context, to empower people and allow them to own and contribute to the system they live in.** The [Making Sense Toolkit](http://making-sense.eu/publication_categories/toolkit/) has been a useful reading in this sense. The project empowered citizens to participate in environmental monitoring and change-making at a local level. Citizens were able to interact with their environment in a whole new way, by sharing information through sensors, and this information was useful in rethinking local policies. The MS Toolkit also provides **four principles for intervening in citizens participations: empowerment, change-making, co-creation, openness.** Indy Johar [defines this particular kind of innovation](https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4) as **societal innovation**, a kind of innovation which works in the interest of public good, and  “it is not limited to collective self-interest of a single community, but includes the interests of those who have not yet been born or arrived or are beyond the boundaries of one specific community.”

* **It helped me defining the topic I want to investigate:** the question of ethics and responsibility, both personal and collective, in designing with new technologies is recurrent in our lectures, and I find myself thinking about it often. I see how technology makes it possible for us to access directly a whole new range of information about ourselves and our environment (in this sense, the DIY BIO labs are one my favourite example), and how at the same time the same information could be exploited by other actors, maybe without us consciously knowing it. An example of this is Amazon, which [recently patented a new technology to empower Alexa to understand our mood and emotions](https://www.theatlantic.com/technology/archive/2018/10/alexa-emotion-detection-ai-surveillance/572884/), possibly to sell targeted advertisement or even medicines in the future.
I believe that no information is more personal and should be owned so directly as health information. But thinking about what I define with health, it is impossible for me to separate it from the concept of wellbeing, which is directly influenced by the environment we live in. **How much the cities as ecosystem influence our health and wellbeing? How many information could we exchange in a city about the personal and collective health and well-being of the citizens? Could we design a new discourse around it, one that allows the citizens to experience a meaningful connection with their ecosystems?**
The role of emerging technologies in this field is already huge, but it is mainly exploited by private corporations or institutions, not by the collectivity.
I want to investigate this topic more, by **understanding how today and in future, possibly self-sufficient cities, citizens can participate in and own their health and wellbeing information to close the existing gap between themselves and the ecosystem they live in**.

### Tentative reading list
*on design for systems, participation, emerging technologies, future cities and personal and collective health*

* **Participating in publics**, in McCarthy, John. Taking [A]part: The Politics and Aesthetics of Participation in Experience-Centered Design (Design Thinking, Design Theory). The MIT Press.
* [Leverage Points: Places to Intervene in a System](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)
* [Societal Innovation & Our Future Cities - The Case for a Social Contract for Innovation](https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4)
* [Putting users first is not the answer to everything](https://medium.com/doteveryone/putting-users-first-is-not-the-answer-to-everything-dd05b9f11b5)
* [Can software be good for us](https://medium.com/what-to-build/dear-zuck-fd25ecb1aa5a)
* [How to Design Social Systems (Without Causing Depression and War)](https://medium.com/what-to-build/how-to-design-social-systems-without-causing-depression-and-war-3c3f8e0226d1)
* [Making Sense Toolkit](http://making-sense.eu/publication_categories/toolkit/)
* [AI offers a unique opportunity for social progress](https://www.economist.com/open-future/2018/09/20/ai-offers-a-unique-opportunity-for-social-progress)
* [CareAi — Mixing Healthcare Data with Emergent Technologies for Impact](https://medium.com/@lucaslorenzop/careai-ideasforchange-26d2baf9e757)
* [Can shared living improve our mental health and wellbeing?](https://medium.com/space10/can-shared-living-improve-our-mental-health-and-wellbeing-ff5498b02d4a)
* [Smart cities: health and safety for all](https://www.thelancet.com/journals/lanpub/article/PIIS2468-2667(17)30156-1/fulltext)
* [What is a healthy city](http://www.euro.who.int/en/health-topics/environment-and-health/urban-health/who-european-healthy-cities-network/what-is-a-healthy-city)
* [The future of public health lies in cities](http://www.who.int/healthpromotion/conferences/9gchp/chan-public-health-future/en/)
