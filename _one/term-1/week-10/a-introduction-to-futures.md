---
title: 10. An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---

I wish I had this class way before in the trimester, because it provided us with a framework to navigate the idea of futures.

## Futures as a cultural narrative

**The future does not exist - it is a narration about how we understand the present and how we believe it will develop.**  We imagined possible futures a century ago, and we are now living in the result of those speculations. The current present is the result of a past narrative about the future, the result of previous projections that created possible, plausible, preferred and probable futures.
The idea of **future is shaped on shared cultural beliefs and  perceptions of society**(of its progress, wellbeing, economic outlook, diversity). In other words it is based on our **worldview**, or **the way we process information from our brain to our mind, evolving it from perception to a thought.** Knowledge and ignorance are an integral part of our worldview, because what we know and what we don't  equally participate in forming our point of view on reality.   **Current trends and drivers** are crucial in building our idea of future, because they take it directly into consideration.

We can **study the future as we thought of it over time**, meaning from a historical and cultural perspective. To analyze the idea of future at a specific moment in time means to observe that time and identify the main forces driving that particular interpretation. The process could be done in reverse as well, meaning that we can interpret a specific historical period by looking at the idea of future that was dominant.

I've always been a fan of science fiction, and I've always been generally fascinated by representations of possible futures in culture. **Creating a future is a speculative and imaginative effort, and it also embodies powerful reflections on the present.**
**Jules Verne's stories** tell us about a time when technological optimism was dominant in the European culture, so much that he thought we would have conquered the planet in every way (inside it, under the sea level, in the sky). Verne was so curious, so enthusiastic about the scientific discoveries of the 19th century that he enjoyed describing every possible future invention in detail, from how life on the Nautilus was managed to how the man would have reached the Moon. **Ray Bradbury's Martian Chronicles** contains at the same time a reflection on the American society in the 1960s and a speculation about what could happen following the race to space.
What will the **movies written by the Wachowski brothers** tell to future humans about us? One of the recurrent themes in their films is the loss of empathy and compassion. Speculating on it, the brothers imagine that humans will experience this loss in the near future, so much that our species will reach the point of being endangered. In Matrix we will lose it to the point of being dominated by machines. In Cloud Atlas technology will alienate us so much that we will create slaves/clones and feed them with the corpses of other clones.
We see in Cloud Atlas how empathy and compassion are the two forces keeping us human, how the exercise of understanding what is "other" allows us to be saved through different times and historical situations. This message of openness is a powerful reflection on our present, where complexity is a dominant force and making sense of reality is more difficult than ever.

## Futures as speculation
Because the idea of future is built on a cultural narrative and on our worldviews, the speculations we can do about it are **limited** by our knowledge and ignorance and personal perspective on culture and trends. Forecasting is a kind of speculation, and is an exercise to both understand our present and drive possible positive future changes.
Because **we see time as linear but we cannot assess the present with the same linearity**, understanding how futures are created is useful to understand our complex, chaotic and contradictory present and make sense of it.
As for driving possible positive changes, forecasting futures let us envision **preferred futures**, which should be [invented, implemented, continuously evaluated, revised, and re-envisioned](http://www.futures.hawaii.edu/publications/futures-studies/WhatFSis1995.pdf). It also let us escape from [the trap of shortermism, which prevents us from tackling huge, systemic world-scale issues](https://ideas.ted.com/three-ways-to-think-about-the-future/).


### Understanding the present - Postnormal times
In our present time, we live with the consequences of the [progressive dying of modern ideologies: we are losing confidence in the values that built our western societies and states](https://www.economist.com/open-future/2018/10/08/are-liberals-and-populists-just-searching-for-a-new-master).
The speed of information sharing in our systems has increased since the globalization of the internet. News can escalate in relevance in a few hours, and the consequences of this escalations have unpredictable systemic impacts. **The audience of any information has increased in size enormously**, and this influences how facts are defined: different worldviews may clash in assessing information, and defining truths and knowledge is increasingly difficult. The more we know, the faster we change, as Yuval Harari said in his latest book Homo Deus: "The more we know, the less we can predict. This is the paradox of historical knowledge. Knowledge that does not change behaviour is useless. But knowledge that changes behavior quickly loses its relevance. The more data we have and the better we understand history, the faster history alters its course, and the faster our knowledge becomes outdated".
A possible definition of our time is **postnormal**. Postnormal times are defined by **chaos, complexity and contradictions.** These element permeate systems as a whole: interdependent relationships and complex networks are characteristics of postnormal times as well.
For this reason, dichotomies are not anymore a valid instrument to evaluate present and future systems, and thinking outside of our cultural knowledge box is the exercise futurologists do to assess the present and identify signals of possible futures.

### Forecasting the future

How to identify possible futures? To find signs of familiar (foreseeable) or untought (outside our worldview) futures in the present, we have been provided with a framework:
* **Black swans**: defined as elements outside and beyond our normal observations, black swans are unimaginable outliers that may have a positive impact on systems, helping us imagining new positive familiar futures.
* **Black elephants**: defined as widely predicted elements, black elephants are ignored events pertaining to postnormal times. They are known and unknown at the same time, and when they happen they are treated as black swans, even if signs of them were identifiable.
* **Black jellyfishes**: defined as unforeseeable elements, black jellyfishes are events with the potential to escalate rapidly due to systemic instability. They belong to untought futures.

To be able to assess complex systems and identify these signs, we have to **unlearn to see the future**.
Events and information evolve in time, changing. Because we live inside this evolution, we fail to notice changes and judge events and information by using outdated categories. We avoid doing the exercise of thinking outside of the box and we judge possible futures by present or past standards.

One of the reasons I liked this week so much is because it provided an explanation, a framework to assess present trends. I believe that much of the emergence of political right extremism all around Europe have to do with a **general inability to make sense of systemic complexity.** Complexity creates fear and right-wing politicians are very good at giving simple, yet concrete answers to this complexity by basically saying "let's eliminate it". Their political agendas are very well designed in this sense, their opposition to chaos is concrete. At the same time, the traditional left is struggling in developing strong answers to complexity, and it's failing in tackling the challenges we are facing. I think at Italy, and two examples come to my mind. The first one is about new technologies: while Matteo Salvini, the most prominent right-wing politician, has embraced it and use it well (he maps his Facebook audience, [building his political agenda over it](https://www.ilpost.it/2018/12/10/matteo-salvini-fascismo-estrema-destra/)), the left is lost in making sense of it. Major centre and left Italian politicians, clearly not accustomed enough with the issue, foster irrational fears by advancing the possibility that "new technologies will steal people's jobs" and fail in providing an answer to this (false) threat. They are lost in complexity and unable to shape a political agenda that takes into consideration the contradictions of our time. The same dynamic happens in Italy when immigration is discussed. While the right face complexity and give a political answer to it ("let's not have migrants"), the center and left are unable to shape a positive political message ("let's accept immigrants")that takes into consideration the complex issues related to immigration.

## Possible futures in my area of interest

To identify future consequences of a systemic trend, we made an exercise with the **Futures Wheel**. This framework helps considering possible first and second level impacts of the emergence of a change. In class we exercised with veganism and noticed how, when impacts are connected to one another, complexity is generated. I tried to do the same exercise with a topic related with my area of interest, which is **personalized health**, or a medicine that is tailored to the unique needs, genetic makeup and lifestyle of each patient.

![]({{site.baseurl}}/Health future wheel.jpg)


## Readings on future as a cultural narrative

* [Fact, fiction, and the future](https://howwegettonext.com/fact-fiction-and-the-future-f5937d468ba6)
* [Cyberpunk - A three-minute, quick-and-dirty primer for Cyberpunk.](https://medium.com/cyberpunk-culture/cyberpunk-f7e392369052)
* [Cyberpunk](http://web.mit.edu/m-i-t/science_fiction/jenkins/jenkins_5.html)
* [Exploring the Future Beyond Cyberpunk’s Neon and Noir](https://howwegettonext.com/exploring-the-future-without-cyberpunks-neon-and-noir-8e23562819e3)
* [Your Brief And Far-Out Guide To Afrofuturism](https://www.huffingtonpost.com/entry/your-far-out-guide-to-afrofuturism-and-black-magic_us_5711403fe4b0060ccda34a37)
* [A cyborg manifesto](http://faculty.georgetown.edu/irvinem/theory/Haraway-CyborgManifesto-1.pdf)
* [Gulf futurism is killing people](https://www.vice.com/en_us/article/7b74wq/the-human-cost-of-building-the-worlds-tallest-skyscraper)

## Readings on postnormal futures

* [Theory of change and the futures cone](https://sjef.nu/theory-of-change-and-the-futures-cone/)
* [How to think like a futurist](https://ideas.ted.com/three-ways-to-think-about-the-future/)
* [Our Puny Human Brains Are Terrible at Thinking About the Future](https://slate.com/technology/future-tense)
* [A new age of strategic innovation](https://provocations.darkmatterlabs.org/a-new-age-of-strategic-innovation-dcf5fa2c9919)
* [Yuval Noah Harari: ‘The idea of free information is extremely dangerous’](https://www.theguardian.com/culture/2018/aug/05/yuval-noah-harari-free-information-extremely-dangerous-interview-21-lessons)
* [Are liberals and populists just searching for a new master?](https://www.economist.com/open-future/2018/10/08/are-liberals-and-populists-just-searching-for-a-new-master)
* [Black Mirror, Light Mirror: Teaching Technology Ethics Through Speculation](https://howwegettonext.com/the-black-mirror-writers-room-teaching-technology-ethics-through-speculation-f1a9e2deccf4)
* [How We (Don’t) Talk About Climate Change](https://howwegettonext.com/how-we-dont-talk-about-climate-change-3e20d9d83053)
* [What Our Tech Ethics Crisis Says About the State of Computer Science Education](https://howwegettonext.com/what-our-tech-ethics-crisis-says-about-the-state-of-computer-science-education-a6a5544e1da6)
* [Silicon Valley Thinks Everyone Feels the Same Six Emotions](https://howwegettonext.com/silicon-valley-thinks-everyone-feels-the-same-six-emotions-38354a0ef3d7)
* [The ID question](https://howwegettonext.com/the-id-question-6fb3b56052b5)
