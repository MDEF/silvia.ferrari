---
title: 04. Exploring Hybrid Profiles in Design
period: 22-28 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---


When I first started taking notes for this week’s post, I made an effort trying to categorize my thoughts, separating what resonated with me at a personal level and what at a professional one. As the week passed, I realized that **when discussing hybrid profiles -  people who mix different skills and knowledge to create unique life paths, it's necessary to perceive a profession and a life as a whole**, because the two aspects influence each other so much that trying to separate them makes them lose significance.

Getting to know other people’s path, I also realized that in the past few years I have been a hybrid profile myself. In terms of skills, attitude and knowledge I never completely fitted neither in politics nor in innovation consulting, but I’ve been adding value to both activities in a circular process: what I learned on one side, I tried to apply in the other, adapting it to the emerging circumstances. I have always struggled for this reason, because mixing competences and experiences is contrary to the very clear and straightforward path that everyone is supposed to follow in the pursuit of a professional life, but meeting Sara, Ariel, Marta and Pau and being able to relate my experience to their stories helped me acknowledging the main features of a hybrid profile, as well as the added value in pursuing this kind of path:

- **There is value in a multidisciplinary approach:** mixing competences is a source of value for oneself and for businesses as well. According to Marta and Pau, Domestic Data’s collection of insights on human behaviour benefits a great deal from the knowledge added to the projects by an anthropologist, who is able to provide his expertise in social analysis and help the company producing detailed reports. In the same way, by studying materials Sara is able to add value to her work as an artist by experimenting with unusual textiles in shoemaking.

-**Diversifying allows lightness:** focusing on one thing at a time does not allow any experimentation, because the risk of failing is too heavy. This mechanism could be frustrating in the long term, because it forces to narrow down one’s interests and, if the outcomes aren’t satisfying, the burden becomes even heavier. I related very much to a part of Ariel story: he became dissatisfied with his consultancy job once the management side of it turned very demanding. He wanted to pursue a more creative career. Because he was focusing on various projects at the same time, he was able to leave the job and modify his career path.

- **Curiosity fuels hybrid paths:** I see a genuine interest in discovering new things at the core of Ariel, Sara, Marta and Pau’s life paths. Curiosity allows them to be unique and effective in the pursuit of their interests. I also realized that to keep curiosity alive it’s important to be stimulated and be close to a perpetual source of new knowledge. Ariel, for example, decided to pursue a PhD and found in university a place to be inspired, while Marta and Pau stated many times that they look for new clients and new challenges all the time because it helps them exploring.

![]({{site.baseurl}}/path.jpeg)

*From Ariel Guersenzvaig slides - a very reassuring way to look at a life path*

- **Hybrid profiles choose hybrid spaces:** I really liked visiting Sara’s and Domestic Data Streamers’ offices. Both allow their owner to play around and shape the space according to their needs. I have worked in an open space, very “Google-style” office myself for the past few years, and I enjoyed the kind of freedom a hybrid space offers. I think that space has a big influence on one’s mindset: flexibility in moving around allows to have a flexible mind. I particularly liked how both Saras and DDS spaces encouraged to think and play at the same time without any clear border between the two activities: in DDS offices the boundary between thinking and making is blurred, and this reflects the nature of their activity, which is both theoretical and practical.

Meeting with Ariel, Sara and the DSS people was enlightening also in terms of **research topics**: confrontation with them helped me understanding my direction a bit more. Sara's and DDS's visit were useful in understanding how practical experience and experimenting can be embedded in a design process. Sara's experience is the most distant from me in terms of education and interests, but I liked her **very open and chilled attitude towards experimenting**, which I guess it could be applied to other fields in the design practice. I also liked very much the DDS approach to data, which mixes theory with practice. In their view, **data are always connected to an experience**. Ariel's research topics are the closest to mine: he is interested in understanding **how design can contribute to society by questioning its ethics**. The intelligent robot dilemma was very interesting to discuss, and it showed me how design plays a part in the definition and solution of contemporary issues. In his words, to **design is to plan something that will happen in the future. It relies on intuition, as well as on the ability to understand the present.**

![]({{site.baseurl}}/moodtest.jpg)
*From Marta's and Pau's slides - meaningful data are gathered from an experience: the mood board was painted on a wall close to Raval and people were asked both to define their current mood and state the size of their houses*


The most striking realization for me was that **to be a hybrid profile is essential to have a clear sense of personal direction**. I feel that in my path so far, direction and ambition are what I’ve missed the most. I realized that, although I am pretty confident in my abilities, I become very insecure when I have to think about future possibilities for myself.
The whole week made me realize that until now I haven’t been able to find or **develop a framework for my personal development that resonated with myself at a deep, meaningful level**, but I feel two activities during last week helped me a great deal in achieving that goal:

- **Defining myself through a list:** by relating to Sara, Ariel, Marta and Pau’s experiences and adding my personal reflection on myself, it has been possible for me to isolate some personal features resonate with me in a meaningful way. I have been a fan of listing stuff for a very long time: I appreciate the very unconscious, almost magical power they hold. By just stating something, you put yourself already on the path of achieving it. In this case, making a list of points helped me seeing myself from outside and define my identity as a designer

- **Learning a new framework to evolve:** the reading list provided for the week offered me a new perspective on learning and producing outcomes from it, one that allows personal motivation to be at the core of every activity performed, in “a process where insight into the design opportunity and solution domain is achieved by continuous information gathering.”(from: *Designing for the Unknown: a Design process for the future generation of highly interactive system and products*)
In this framework, personal direction is defined through the experiences one has and  the ability to gather knowledge from them in a circular process that powers itself. As I learned by readings, **finding a way to reflect on experiences and practices is essential to create more knowledge in the design field. To reflect means, in this case, to abstract significance from a particular situation “to a higher level, in order to produce a knowledge yield that is applicable across a broader range of situations”**. In my case, applying this new framework meant to combine my current learning experience with the skills and features that I listed previously, to create a personal development plan - a direction - for the next few months.

![]({{site.baseurl}}/Silviapath.png)
*Listing items that defined myself and highlighting those which need improvement helped me in drafting a personal development plan for the next few months*


Other than the courses we are going to attend in the next few months, I would like to add to my personal development plan:

- **A reading list**, that I will try to follow to reflect in a meaningful way on the topics that interest me
- **A plan for my daily/weekly activities**, to be more organized and to find the time to actually explore all the topics that I am interested in
- **A list of topics that I am interested in**, and that I would like to explore to shape my direction in the future.

In the long term, I want to focus on the topics that interest me the most, shaping my  identity as a designer towards:

- The **political and economic relevance of cities** in the near future
- **How technology will change our economy**, by creating new jobs, new skills and new production processes
- Circular economy and its **political implications**
- The **possible political frameworks that will allow for a decentralized ecosystem to become increasingly important** and for citizens to participate in the political process in a meaningful way
- New **paradigms for human behaviour and interaction** among people
- The **concept of interface**, how interfaces allow us to understand our environment and how we need them to find a meaning in it

Drafted in a vision, I would like **to investigate how circular economy, technology and the changing balance in political and economic power will change the way people participate in society and how the political framework should be shaped in the future to allow for citizens to participate in a meaningful way**
