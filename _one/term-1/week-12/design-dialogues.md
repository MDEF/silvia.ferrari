---
title: 12. Design Dialogues
period: 17-23 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---

## Defining my idea
When working on the final presentation of my idea, I spent some time reading the submission I made throughout the term, and i was surprised to notice how -since the very beginning- each week contributed to the development of my final concept
In particular, during **MDEF bootcamp** I started reasoning in terms of systems: I realised that we are always part of different systems at the same time, each of us connected by relationships to other people and other systems. **Navigating uncertainty** gave me more insights into systemic thinking and the power of design in defining these systems and making it possible for us to interact between them through interfaces.
**Biology 0** made me realize that DIY BIO offers us a way to interact with scientific knowledge, a field that we have grown detached with. I realized that  reappropriating scientific knowledge is an empowering act for us. **Designing with extended intelligence** made me aware of the growing power of data as a commodity. The week provided me also with an interpretation of artificial intelligence as an *extended intelligence* that I find compelling and that also fits with my interest in systemic thinking. The weeks devoted to **digital fabrication and electronics** boosted my creativity and helped me understanding how to materialise the concepts I discovered while studying and learn through prototyping.

The starting point of my research -during week 5- was acknowledging that we are surrounded by information that we cannot read because we have not still designed the interfaces to do it. There are rules and interconnections in our systems that we fail to notice simply because we don't have the means to do so. It has always been like that for humans: we always imagined bacterias existed, but it is just recently that we found a way to see them.
In the same way, we know since a long time that the environment we live in affects our wellbeing, but we fail to understand this relationship in our newest and most important ecosystem: the city.
How could we make this relationship alive? How could we manifest this flow of information? Could we design an empowering circle by understanding this information and take better decisions -for ourselves and our systems- based on that? The amount of available information on ourselves and the cities we live in is growing in the form of data: we have health and lifestyle data about ourselves, our DNA could be coded, we can sense and monitor the wellbeing of the city as well. My idea is about finding ways to connect this information, make it understandable and meaningful to us and build on the manifestation of this relationship. Also, I am interested in exploring how personal data - especially health data- are acquired and shared today and how we can personally own this process.

## Researching my idea

**Systemic thinking**

We are aware of the fundamental interdependence of all phenomena and of the fact that, as individuals and societies, we are embedded in, and dependent upon, the cyclical processes of nature
The interactions of living things with their environment is a cognitive action. The brain is not the only structure through which the process of cognition operates:  the entire structure of the organism participates in the process of cognition. This is a known-unknown concept in our society. In rural cultures the link existing between humans and the ecosystem is manifest, but what about modern living? The idea has been explored in movies and books, but how do we apply it to our daily life?

**Wellbeing as a systemic indicator**

In the globalized world, the state of society has been measured for a long time through economic indicators. Recently, our understanding of the systems we live in has changed: an emergent trend in economics and policy is to evaluate wellbeing through new indexes, that promote a multi-dimensional and holistic approach to global, national and regional wellbeing. Bhutan created in 1972 the “Gross National Happiness Index”, and since then the European Union runs a “European Quality of Life Survey.” The OECD has a “Better Life Index”, while the United Nations commissioned a “World Happiness Report”. In Italy, the National Institute for Statistic (ISTAT), publishes annually a “BES - Equally and sustainable wellbeing index”, which has a national, regional and urban perspective on wellbeing. The index takes into account 12 wellbeing indicators, (each one of them aggregating many variables): health, education, work-life balance, economic wellbeing, social relationships, institutions, security, subjective wellbeing, landscape and culture, environment, research innovation and creativity, services.

**Cities as our ecosystems**

Having a multi-dimensional understanding of our environment and of how we exchange information with our system becomes more important in light of the fact that by 2050 most of the population will live in urban environments. Cities will soon become our primary ecosystems, including nature and humans in a largely human-built context.  Urban environments impact on our wellbeing: air pollution, road traffic congestion, and lack of safe spaces for walking and exercising all contribute to rising death rates from heart diseases, cancer, respiratory illness, metabolic dysfunctions and injuries. Urban sanitation planning and waste management prevent the transition of vector-borne and infectious diseases. Our ecosystem impact also on less measurable disorders, such as stress, depression and alienation. The constant relationship we have with our city has been explored widely, Still, we lack a deeper, meaningful understanding of  this connection, one that could give us a systemic perspective of the interdependence of all the phenomena happening in a city. These indexes use quantitative and qualitative to create a multidimensional picture of our societies, but they fail in capturing the day-to-day dynamics of our lives. Also, few of them analyze the urban environment closely.

![]({{site.baseurl}}/concept.png)

**The experiment**

To gain more insights into my research I made an experiment: I **lived with my idea for one week**, focusing of the constant communication between myself and Barcelona. To do so, I monitored my health and lifestyle with my phone and mixed these data with environmental data about the city. How did the city impact on me? Having this question in mind made me living with an enhanced sense of awareness.
I also found myself reflecting on the consequences of sharing my most personal data.

*Apps that I used: what am I really sharing?*

![]({{site.baseurl}}/sharing.png)


## Presenting my idea

For the presentation, I chose to design a path showing:
* The context of my idea
* The emergent research questions
* A journey through my experiment showing the insights that I gained
* Possible future areas of intervention for my idea.
I created an A0 file, available [**here**]({{site.baseurl}}/Poster.pdf)

To introduce the concept of my idea, I also chose to present the story that I created during the engaging narratives week. The story is fictional and serves as an introduction to the idea of being able to understand our environment in new, non conventional ways. The story is available [**here**]({{site.baseurl}}/This is Amelia.pdf)

I also added a short bibliography, available [**here**](https://mdef.gitlab.io/silvia.ferrari/readings/)

Developing a presentation was a really useful exercise for me: in the process of doing it, I realized that I spent most of my time focusing on the contents, without caring too much about how to show them or the interactivity, missing a creative opportunity. Next time, I am going to focus more on those aspects.

## The feedbacks I received
The presentation was challenging but very useful as well. I managed to have long talks with most of the people serving as a jury, having the chance to explain my idea in depth. I received positive and useful feedbacks on the presentation, suggestions for future developments of the idea and many new references to look at.

* **What to focus on**: one of the core points of my idea is to create awareness about the relationship we have with the city through data. It was suggested to me that it would be interesting to explore *what comes after data collection and an enhanced awareness of the interconnections between us and the city*. Which kind of interventions could we imagine? Jonathan suggested to think about possible development in this direction **subverting the known and the unknown, interrupting a known system by turning it into something unexpected**.

* **What could happen after**: I received useful suggestions on how to develop further my idea. In terms of contents, *data literacy* emerged as a topic that I could explore deeper: how can data talk to us? Which kind of information ot message it can give us? How can we be aware of how information affect us? Another topic that was suggested was *the habit of tracking*. Why do we do it? Which are the consequences of it? Who is this information most useful to? Us or the companies that provide us with the apps and wearables to do so?
In terms of structure, it was suggested to deepen one or two of my research questions, extending my experiment (also focusing on **how data visualization affect the exchange of information and how it encourage/discourage change**) and creating use cases based on that. Also, it was pointed out that I could develop a methodology to prototype my intervention.

* **Fab Academy**: I was asked what I would like to produce in the Fab Academy in relation to my idea. I explained that I thought about creating an interactive object based on my concept: for example something interacting with us and data about the city and ourselves. It was suggested to me that I could have a different approach to the Fab Academy, using it **as a tool to learn how to communicate**. Instead of focusing on one object I could experiment with the different digital fabrication techniques creating small interventions around the same topic.

* **The presentation**: It was pointed out that the the methaphor I used to explain my idea -becoming urban shamans - could be the wrong one: shamans deal with the unknown, while we know our data. I used it because I fint it meaningful, but I will work on improving the metaphor to make the idea more understandable.

### State of the art in my field - suggested resources

* [Richard Sennet / Building & dwelling - Ethics in the city](https://www.amazon.it/dp/B075QJYQTM/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1)
* [Salus.coop](https://www.saluscoop.org/)
* [Triem](https://triem.herokuapp.com/en/welcome)
* [Making sense toolkit](http://making-sense.eu/publication_categories/toolkit/)
* [Arturo - Un algoritmo entrenado por ciudadanos para disenar ciudades mas abitables](http://arturo.300000kms.net/#2)
* [Vienna summer scout](https://johannapichlbauer.com/summer-scouts)
