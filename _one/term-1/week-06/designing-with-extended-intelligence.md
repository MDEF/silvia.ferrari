---
title: 06. Designing with Extended Intelligence
period: 5-11 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

The week we spent exploring extended intelligence was aimed at sharing with us the fundamentals of intelligent machines, how they work in practice and the emerging ethical issues in designing extended intelligence.

During the past week, we learned that to design is never a neutral act. This same reflection recurred in the debates on extended intelligence: **by designing algorithms and leveraging on machines, we are connecting perceptions and symbols. Defining something-that-is is, grounding it by categorizing it is an act of mediation which brings along a certain view of reality and its connected biases.**

I feel that I am still exploring the concept of extended intelligence. I understand that this technology could be applied towards positive innovation, could in a way help us open new scenarios by connecting us with information in a brand new way. At the same time, I think that there is a general negative attitude towards it, which influences the way machine intelligence is communicated to the public, making the debate very polarized between optimistic and pessimistic opinions. Gathering knowledge on this topic is a constant act of demystification of very general, misinformed and manipulated information.

To help us grasp the fundamentals of extended intelligence, we worked throughout the week on imagining a machine intelligence (its tasks, goals, biases..) following what we learned in class. The exercise was really fun and useful, it helped me highlighting the main challenges and contradictions in designing with artificial intelligence  

**Structure**

The structure of machine intelligence is rigid. Often, we fail to remember that the process of an intelligent machine is always connected to a very precise purpose. We tend to imagine a future where artificial intelligence will behave like us and, in the gloomiest scenarios, will be unrecognizable or take control, but **we never rationally deconstruct our fears. While we imagine infinite, very sophisticated machines, we never ask what such an outcome would entail. We fail in doing this exercise and therefore we fail to see the structure on which artificial intelligence is built.**
The main AI characteristics are its reliance on data, its ability to learn as opposed to being programmed, its narrow scope and focus and the fact that is a constantly evolving field.
However, the term artificial intelligence is debated. What does intelligence means?

*For our group exercise, we had to define what is an intelligence. Our group agreed on identifying intelligence as a cognitive action, working on a reasoning process (or the ability to select information, link and process them to produce an outcome). The ability to perceive the environment and select information from it (at various level, from our unconscious to our senses) is a feature of intelligence, as well as memory. Learning is also an ability connected with intelligence. To sum up,* ***intelligence is always directed to a purpose and involves decision making, which is a process of selecting information towards a purpose***.

[Joi Ito discuss intelligence by taking into consideration its networked feature](https://pubpub.ito.com/pub/extended-intelligence): “Hasn’t intelligence always resided beyond any single mind, extended by machines into a network of many minds and machines, all of them interacting?”
**Extended Intelligence** offers a different way to look at AI by **understanding intelligence as a distributed phenomenon, one which has always been present in the history of human-kind.** Because machine intelligence is such a debated topic and because its functioning involves such fundamental ethical issues, I think it’s necessary to be as clear and informed as possible in the definition each of us gives to it. Personally, I like to see it from a collective point of view. As an Extended intelligence, **AI can be considered the new technology with which humans can contribute to their collective intelligence.** As a tool that helps to expand our knowledge, [the aim of its design and purposes are not so different from the Memory Board](https://aeon.co/ideas/this-ancient-mnemonic-technique-builds-a-palace-of-memory) we discussed last week.


**Limits**

The process of selecting information and producing outcomes based on it is at the core of how machines learn. The concept used in artificial intelligence to define this process is the rational agent, an agent which acts in a way that is expected to maximize to its performance measure, given the evidence provided by what it perceived and whatever built-in knowledge it has. A rational agent is able to adapt very fast to dynamic external condition, and its rationality is measured by performance measure, the prior knowledge it has, the environment it can perceive and actions it can perform.

There are limits to its ability to perceive external inputs. Social algorithms, for example, are limited in their ability to understand emotions because of a symbol grounding problem. To make an algorithm work, it is necessary to take a stand and associate values to the inputs the machine receives. Which value does define happiness? How do we collectively define happiness? Is it possible to do so? The answer is very personal and, for this reason, the outcomes of such an intelligence are biased and limited.

**Design with extended intelligence**

The practice of designing with machine intelligence is still very much an exploration. How it is possible to design trustworthy, reliable intelligent objects? There is debate over the scope of design as well as over the methodologies to be used to gather information and build intelligent objects. There are also different ways in which the machines can learn, and this influences the outcomes an AI can provide.

*For our group exercise, we had to define an intelligent object. Imagining such an object requires to define its goals, tasks, inputs, outputs and context of operation.
Our group decided to create a tool that would help us in organizing our visit to China.
+ Goal:  to plan a trip for specific dates with a specific budget
+ Inputs: budget, dates, number of meals a day, type of accommodation, personal preferences gathered from a profile, transport to and from the destination, local transports, category of trip (ex. food, culture)
+ Context: community-based platforms that give recommendations for trips (ex. TripAdvisor)
+ Tasks: collect the information from the context, analyze the data, analyze the profile information, relate such information with other analyzed data
+ Outputs: the best possible trip
Example: to plan a trip to Shenzen from the 8th to the 14th of January 2019 for study reasons with a budget of 1000€
We decided that our AI would learn by using unsupervised machine learning: in this way, it would learn by identifying shared features in the dataset and reacting to their absence. We also decided to use artificial neuronal networks to add a new feature to our intelligent object: the possibility to predict possible trips for the user by learning on past preferences.*

Design is never neutral and designing with machine intelligence is challenging in terms of ethics, with safety, fairness, justice and discrimination being the most impacted areas

*For our group work, we discussed the possible ethical biases of our intelligent object. We discovered that even if we designed a very simple application, it would be biased, mainly in terms of discrimination(because not everybody has internet access/access to the platform) and safety (because of the large amount of private data being involved in our process)*

I am definitely still exploring around the concept of extended intelligence, its possible applications, its biases, but the more I go forward, the more I understand the power of design in shaping our experiences and preferences.

For example, I am bothered by one of the central ethical issues in AI: how by delegating our decisions to machines we scale down our free will. [Anticipatory design especially could affect our ability to take truly independent decisions or change the patterns we live by.](https://uxdesign.cc/the-power-and-risks-of-anticipatory-design-14ee45efa8b0) While I understand how AI could be useful in downsizing the load of decisions we have to make every day, I am struggling with the definition of a  boundary for it, to avoid delegating too much, pursuing an increasingly easy life and becoming [like the people from the future in Wall-E](https://www.youtube.com/watch?v=ohcwksrvDOg).

Creating a reliable, trustworthy and efficient AI in real life requires to adapt knowledge of the mechanisms behind it to existing design methodologies. For example, an emerging issue in designing with AI is the difficulty in prototyping ideas: because of the amount of data required to create a functional prototype, machine learning requires a very high level of design commitment, in contrast with the core idea of prototyping which is to create fast and learn by doing. **Existing patterns of human-computer interactions are maybe insufficient to work with AI, and the process of defining new ones requires to understand the aim of our actions.** Should we design better computers that work like people? Or design them to work better for people? Again, **imagining machines as an extension of our intelligence instead of something that is other-than-us helps in giving a sense of direction.** [It has been pointed out](https://medium.com/digital-experience-design/free-ebook-about-ai-design-822759215f6c) that “technology is neither bad or good, it is a reflection of the human that is creating it”. The more people are involved in using machine intelligence, the more we will be able to feed an extended intelligence with data, making it less biased as a result. In my exploration around Extended Intelligence, these are the principles I am starting with.
