---
title: 07. The Way Things Work
period: 12-18 November 2018
date: 2018-11-18 12:00:00
term: 1
published: true
---

This week we have been introduced to the principles of physical computing by putting our hands on a variety of electronic devices. We learned how electronic appliances really work and we designed and coded an electronic network of installations using Arduino.

### The context

I've always liked building and breaking stuff, and I've always admired people like [this guy](https://www.forbes.com/sites/greenbrandon/2017/08/16/made-in-china-meet-the-guy-who-built-an-iphone-using-only-shenzhens-cell-phone-markets/#23ee91366cae), who take time to experiment with electronics and have the skills and equipment to do it.

Dismantling a printer/ scanner was a nice **exercise in awareness about the stuff that we own.** We don't pay enough attention to our electronic devices and by doing so **we fail to notice the stories embedded in them.**

We don't see for example how an object that is meant to facilitate and speed up daily tasks such as printing paper takes time to be produced. Because it's an electronic device, we are driven into thinking it's the outcome of a very standardized and automated production. By dismantling it, I noticed that many small parts (sensors, boards, cables) were kept together by duct tape. **At the beginning of the story of an electronic device there's manual labour.**

Dismantling an electronic device allows us to recycle many of its sophisticated parts. With a little knowledge of electronics, we can use the sensors, gears and leds for new purposes. However, some of the electronic parts are protected by patents. Some microchips cannot be hacked and subsequently cannot be repurposed. Electronics is mostly made in China, and this made me reflect on how [knowledge and production are a very powerful geopolitical tool](https://motherboard.vice.com/en_us/article/qv9npv/bloomberg-china-supermicro-apple-hack). As consumers, **we accept the opacity of the technology we use**, while we should be more aware of the stories embedded in our devices. Open source is a movement that allows us to be more transparent by sharing knowledge, but awareness could be raised also by [creating art advocating for more transparency, with the aim of exposing truths about technology that are just underneath the surface](https://shantellmartin.art/work/printed-circuit-boards/)


![]({{site.baseurl}}/dismantling.jpg)
*The dismantled printer/scanner*

### Physical computing

Before diving into our week's project we received lessons on electronics, wi-fi networks, protocols and the tools we could use to interact with our physical world by using electronics. All these information allowed me to break a knowledge barrier: finally, I really understand what a protocol is, how the internet works, what is a ram.  Protocols, for example, are rules to define how we are going to communicate. To transmit the position of a node/an information, we use these rules and then we encode them. As we are transmitting digital information, everything is either 0 or 1. Each encoding action is a 0 or 1. I enjoyed the exercise we did to understand how a protocol helps you in **locating information** by:

* Taking a piece of paper
+ Drawing a point on it
+ Folding it vertically and assigning 0 to left, 1 to right
+ Folding it again for at least five times giving coordinates about its position by using 0 and 1

--> the encoded position of my point was: **1.0.1.1.0.**

Also, the week allowed me to understand **how to create new relationships with the environment by sensing it**. In other words, we learned a way to create **new interfaces**.  To me, the most important piece of knowledge is that, **to interact with a networked technology and space, we have to always think in terms of inputs and outputs**.

Inputs are the elements we read from the world (for example temperature, air quality, movement, face recognition). These elements, collected through sensors, need to be translated into values to be understood by the other devices of the network. A board connects all the devices and, through a code,  instruct the parts on their tasks. The value connected to the input is received by the board which connects this value to an output, triggering a reaction.

![]({{site.baseurl}}/flow.png)
*A system of inputs and outputs and the commands connecting them*

### I/O Project

The week's project was about creating a network of interacting inputs and outputs into our class. To achieve our aim, each group had to design and create an electronic object. We followed these steps:

**1.Defining the purpose of our object** - our group decided to create an object with no useful aim other than having fun. We didn't want to focus too much on the purpose of the object as much as on the creative effort behind physical computing. We wanted to be free enough to learn as much as possible on the topic.

**2.Brainstorming about our idea** - we discussed the elements we could sense in our room: light, temperature, quality of the air, movement, sound, time. We decided to create a device that would **react** to sensing. In other words, **an output**

**3.Adding fun** - we also discussed how to stick to our purpose, that was to just have fun. Our device shouldn't be used for particular purposes, it should just create an entertaining moment. We wanted to use a LED strip because it is such a beautiful electronic component. In the end, we decided to make a device that would react to the noise in the room by lighting the LEDs and pumping air into a balloon, that would in the end explode.

**4.Creating the outputs**- the first thing that we did was to focus on the creation of the two outputs we imagined: a pump for the balloon and a led strip which changed colours.

**Making the pump** required to create a small circuit by connecting external power and a relay (answering to the amount of sound received) to the pump and the wifi board. We prototyped the idea using different pumps to understand how much would it take for the balloon to be inflated enough to explode. For our first attempt we used a small pump, and inflating the balloon took five minutes. However, when it finally exploded, everybody laughed. Our goal was accomplished, we managed to create entertainment by sensing the environment.

<iframe src="https://player.vimeo.com/video/302272015" width="640" height="1144" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

 *Slow motion video of our first balloon exploding*

**Creating a colour scheme** required to connect the led strip we chose (a NeoPixel Adafruit) to external power (by soldering a prototype), as well as finding the right code to instruct it to blink and change colour according to the sound.


![]({{site.baseurl}}/soldering.jpg)

*Soldering the prototype*


![]({{site.baseurl}}/ledstrip.gif)

*Trying the prototype led strip*



I focused on this task, and I enjoyed very much the search for useful information online. Thanks to the many open source contents that are provided, it was possible for me to learn many useful tips. I liked very much [this open source tool](<https://adrianotiger.github.io/Neopixel-Effect-Generator/>), which creates an interface to create colour codes for the led strip we were using.

**5.Putting everything together** - sadly I missed the last day of class and I wasn't able to participate in the final part of the project, which required to connect together the inputs and outputs created by the different groups in the class. However, I was updated by my classmates on the process and outcome of our device. The input to which our device responded changed from sound to movement. Each time the door of the class was opened, the movement triggered the pump to start blowing air into the balloon and the led strip to light up until the balloon blown.
