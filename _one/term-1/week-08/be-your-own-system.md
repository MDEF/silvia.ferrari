---
title: 08. Living with ideas
period: 19-25 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

The core concept underlying this week's activities is that by embodying ideas into objects and interacting with them in time, it is possible to gain useful insights into that idea.

* By taking a concept we have in mind in the present we can **observe** it
* By taking ideas out of speculation and materializing them, we can **experience** them
* By making an idea present we can stop questioning "what it would be like?" and start speculating on **"what is it like"**

By observing, experiencing and living with a material object we can reflect on **our reactions to it**, on the **feelings that it provokes**, ultimately on the **value, scope and validity** of it. The exercise has been also really useful for me as a tool to **break the creative barrier between thinking and acting**.

### 1st exercise - Every idea can turn into an object
How can we modify an object to provoke a reaction to it? how can we use this technique for our projects?

Everyday objects have been provided, and we are asked to choose one and work with it to create a counterfactual object.

We experience the object, and let it inspire us in answering the question:**" What if...?"**

**What if we couldn't speak anymore?** I imagine a world where we could not communicate verbally anymore. It could be a post-apocalyptic world or, as in the movie [A quiet place](https://www.imdb.com/title/tt6644200/), speaking freely could have become too dangerous. I've chosen a candle, and I imagine a time where we could not use electricity to power our devices anymore.
What would we do?

How would we communicate among us if what we know is alphabet and words, but speaking them would have become impossible? In other words, **which interface would we use to exchange information?**

![]({{site.baseurl}}/device.png)

*I imagine a portable device, made up with a candle, that would allow us to send smoke signals in the day and morse signals in the night*

As soon as I started making my portable device, I realize that, in my fictionary world, we would soon stop using it the way it is designed to. It takes too long to send smoke or morse signals, and we would start communicating in other, more immediate languages. We would maybe use the candle wax drops, or the heat it produces, to allow spontaneity.

**What did I learn from embodying my idea of creating new interfaces to communicate? How this can improve my project?**

+ When imagining new interfaces, we have to **imagine new languages as well**. The candle device is a very inefficient tool to exchange information, and we would soon change it
* **Our habits change** according to the environment we are in and the boundaries we have been provided with
+ **The most efficient solution to an issue is never immediately designed**: we try and each time we close the gap between our idea and the solution we need

### 2nd exercise - Embodying an idea without the technology we imagined

How to embody an idea that still cannot be materialized?

The example we have been provided by Angela is about working on a dynamic fabric that visually changes according to computational inputs.

Such a fabric does not exist, but **we can still envision it existence by creatively using existing technologies.**
In her case, dynamic textile patterns were taken from videos/interesting objects and then turn into animation with a chroma key app. This allowed also for compositions to be created, for example adding a dynamic pattern and making it interact with the material and texture of the fabric she wore.
By creating these animations, **by making her idea present**, she could also notice how such a fabric would have been perceived by those around her. Her friends started "hacking her" by using the same app and adding funny patterns to her dresses.

**What would I wear if I could wear a fabric that acts like a computer screen or a fabric that could change dynamically?**

I would definitely wear colours according to my mood, and would also make me invisible to avoid social interaction when I feel overwhelmed.

![]({{site.baseurl}}/colours.gif)
*My mood today and trying to be invisible with Nico*

In the world I imagine, we would all wear dynamic colours according to our mood, not to show it to others, but to help ourselves be more at ease, concentrate or even [be more creative](_reflections/term-1/week-08/lichtenfeld2012.pdf).
I start questioning myself. Would thinking **"What colour do I feel today"** become a habit for us over time? Also, some social environments such as Instagram and Facebook - which promote sharing and openness towards others - actually [make it more difficult for us to be honest](https://medium.com/what-to-build/dear-zuck-fd25ecb1aa5a). Would a word with dynamic colours drive us towards a less honest interaction or would it encourage us to look for meaning in the way we share our inner thoughts?
Would such a fabric be exploited in other ways, for example, to advertise products?

**What did I learn from embodying an idea into a non-existent technology? How this can improve my project?**

+ It is possible to **embody the essence of an idea** even if the technology does not exist. It requires a lot of effort to identify the core of an idea, but settling on a very specific task it is possible to materialize it creatively, as we imagine to do with a technology that still does not exist.
+ Colors could be a useful tool in the developing of my project, acting as a **shared language** that could help people putting elements in a context where they could understand information
+ Colors could connect inputs with **immediate** outputs. As I learned in day 1, the speed of an interaction is very important.
+ It matters to me to design **meaningful interactions**.

### 3rd exercise - Addressing a need through an object

In the last exercise before the final days, dedicated to our projects, we tried to embody a need into an object that would address it. The core concept was to try to focus on the solution more than the mean through which the need was addressed. This is why we were told to imagine and produce **magical machines**

My first magical machine addressed **order, or the need to create predictable environments**. I have been working on planning my weeks for some time now, to be able to have the mental space and time to study and take advantage of this master's experience in the best way possible. I need a routine, and I've been experimenting with methods to achieve it, including making lists and planning my time. But how do I decide how to allocate it? I need a tool that helps me addressing my weekly needs and experimenting with my time. It's better to study in the morning? To cook at night? How many hours do I need to complete an assignment? I still don't know, and I want to discover it.

I created a **magical dice**, or the **Week Maker**. This tool magically fulfils my wishes by allocating time in the right way to achieve my weekly target.

![]({{site.baseurl}}/weekmaker.png)

*The Week Maker concept*

![]({{site.baseurl}}/usingwm.jpg)

*Using the Week Maker*

In the process of creating it, I discovered that it was very difficult for me to narrow down six categories of activities to write in the dice's faces. I learned that I still find it difficult to understand how to categorize my time.

The second magical machine had to address a need related to our project's idea. I choose **curiosity, or the need to gain knowledge**. My project's idea is about **perceiving what surrounds us in a new way, understanding how the environment affects us and the need to exchange new information previously not shared**. Ultimately, it's about gaining new knowledge.

I created the **Discovery Glasses**, a tool that helps me see what is not watchable by helping focus on something new.

![]({{site.baseurl}}/discovery.jpg)

*Trying the Discovery Glasses*

In the process of creating them, I realized that these glasses don't necessarily work with the sense of sight. Using their magical power, they could help me concentrate on other senses and helping me perceiving new elements. **For things to affect us, it is not necessary to see them**.

### Working on my project

As stated [previously](https://mdef.gitlab.io/silvia.ferrari/reflections/navigating-uncertainity/), I want my project to explore cities and their influence on the citizen's health:

> How much the cities as ecosystem influence our health and wellbeing? How much information could we exchange in a city about the personal and collective health and well-being of the citizens? Could we design a new discourse around it, one that allows the citizens to experience a meaningful connection with their ecosystems?

For this week assignment, we have to test an idea related to our project using one of the strategies that we tried out in the previous days. These include:

1. Living with an idea
2. Materializing an idea
3. Taking a personal perspective on something
4. Creating a speculative artefact

Focusing on what I've learned so far this week, I realized that I gained new insights into my possible project

+ There is information we are not able to catch because we don't have the tools to understand it, but for it to affect us it is not necessary to see it
+ Meaningful interactions matter to me: the way I mean it, a meaningful interaction speaks a language that we immediately understand

I would like to test how we could perceive and interact with the city, I would like to explore how to create meaningful interactions, but I realize that I don't have enough knowledge on how exactly a city affect us. I can imagine some quite obvious relations, for example how pollution and noise have an impact on us, but I feel I could discover much more by **living with my idea**

This is why I decide to work on **The Awareness Project**

*My idea is that our ecosystem is the city, and cities influence our wellbeing and health through urban planning, the air that flows in the city, the natural elements, the amount of space we can use, the organisms living with us in the city.
Interacting with the city affect our body and mind but we don't have an interface helping us being aware of that.
Are our interactions positive or negative? Do we take advantage of the constant exchange we have with the city? Are we informed enough to protect ourselves from possible harms?*

To test my idea, I lived for a while with an **augmented sense of what surrounds me in the city**, trying to notice the many ways in which the city has an impact on my health and wellbeing.

The experience provided me with a lot of new questions about the relationship between us and the city:

* Can I experience Barcelona walking? How many steps can I take? Is it good or bad for me to walk in the city?

* How much city air do I breathe every day? Cities are polluted, but in Barcelona we have the sea. Is it good or bad to breathe? Do we have enough open air?

* Which bacterias am I interacting with? Which of them define my experience of the city the most? Would it be better or worse to create a more close and meaningful interaction with them?

* How do we relate with plants? In how many ways are plants in a city good to us? Is there any bad influence of plants on us?

* Do light influence us? In how many different ways? Is the sun always good?

* Which elements are influencing my sleep? Is it just the noise? And how noisy is Barcelona? Is it only bad?


Ultimately, I realized that I have to understand what do I mean when I talk about health and wellbeing. I also realized that it is difficult to define a boundary between us and the city, meaning the place where the exchange of information happens.

The experience helped me acknowledging that the next steps of my research must include:

* A definition of health and wellbeing: this could include researching about the indexes that many organizations publish every year to assess the quality of life. How do these indexes define health? Which knowledge do they use in assessing? Which data do they use? Which is their ultimate agenda in creating these indexes?
* An exploration of the concept of boundary: where do "Me" end and "the city" starts?
