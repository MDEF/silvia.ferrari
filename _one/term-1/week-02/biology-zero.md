---
title: 02. Biology Zero
period: 8-14 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---
#02. BIOLOGY ZERO

## DIY Biology

DIY Biology is a social movement, inspired by the open source and the "makers” way of thinking, allowing individuals and communities to practice science with the same techniques used in laboratories. DIY Bio relies on everyday objects and innovative technologies such as 3D printing and Arduino: most of the machines used, such as microscopes, are home built. In this way, instruments are cheaper and scientists can personalize them while reproducing a professional laboratory in a non-professional environment.  

### Why it is important

DIY Bio offers a way to introduce biotechnology to the public and connect people to science, allowing everybody to participate in scientific research. Today, we perceive scientific research as something very distant from our everyday life, a practice that belongs to professional labs or corporations, on which we have little control. Historically, though, scientific knowledge belongs simply to those who were curious enough to make experiments. Capitalism and the costs of practicing scientific research individually have maybe contributed to isolate the scientific practice from the public, but DIY Bio offers a way to reconnect to it.
To me, the benefits of a wider access to scientific research are

- **Education:** knowledge is necessary to dive into the complexity of our times, to avoid irrational fears and be able to make conscious, informed decisions on (not so distant) future problems such as genetic engineering. CRISPR technology now allows us  to create genetically modified pigs, whose meat is low in fat. Where do we stand on the matter of genetically engineered humans? Individually, we should be able to answer this question, but to do so we need to understand the scientific context. Also, knowledge protects us from manipulation and the politics of fear, played by populists politicians and decision makers.
- **Openness:** a greater involvement in scientific innovation means a wider participation to the research and development of new solutions to problems. Circulation of ideas is crucial to develop innovation, which benefits the public and creates new opportunities for everybody.
- **Contamination:** access to scientific research allow people from different backgrounds to incorporate biology in their practice, creating innovative solutions at the intersection of different subjects. To design a circular approach to the production system, it is necessary to adopt a more systemic approach to problem-solving, one that takes into consideration the hybrid nature of the global issues we are facing. For example, biology and design together produce a new field, where biomaterials and their possible applications are explored. Other examples include the various studies performed on slime mould, which expands and reproduce itself in space in such an efficient way it has been studied in [various decision-making studies](https://www.vox.com/science-and-health/2018/3/6/17072380/slime-mold-intelligence-hampshire-college)

![]({{site.baseurl}}/plastic.png)

*Examples of bioplastics*

### DIY Bio knowledge

During the course, we focused on molecular biology, microbiology and cellular biology.
We explored the mechanism of life starting from chemistry and the functional elements that, combined together, create molecules and produce the essential biological processes of life:

- **Lipids:** formed by glycerol and fatty acids, lipids form membranes. Membranes are essential to life because they create isolation, separation of spaces and consequently the creation of small ecosystems. Cells for example are surrounded by a membrane, which isolates its elements from external interference.  Membranes are formed by chains of amphiphile molecules, which have both hydrophilic and lipophilic properties: loving both water and fats, these chains interact with different environments, separating them and at the same time allowing certain elements (such as hormones, which are lipids) to go through them.
- **Proteins:** formed by amino acids, proteins are the functional and structural pieces working in the biological processes: they perform specific biological tasks. The amino acids forming them have 21 functional properties (for example some bend, some others are hydrophobic), on which all life is formed. These amino acids, combined in chains, form proteins. Proteins are organized in four different structures (primary to quaternary) according to the way they fold to perform their biological tasks. When a protein is not correctly folded is called a prion, and it will not be able to perform its task correctly, causing issues in the system (the mad cow disease, for example, is caused by prions)
- **Nucleic acids:** composed by nucleotides, nucleic acids provide the information to the biological processes, and the way to transfer this information to all the elements involved.
Nucleotides are formed by a nitrogenous base, carbon and a phosphate group. The five nitrogenous bases that allow the sharing of information are Purines called cytosine, guanine, uracil, thymine, adenine. At a structural level, these nitrogenous bases allow the nucleotides to bond with each other by sharing a hydrogen bond, forming the DNA.
- **ATP - ADP:** A sixth nucleotide, Adenosine Triphosphate, is the one responsible for creating chemical energy in the cells through the ATP - ADP process, which consists in the addition or removal of the phosphate groups forming the structure of the molecule. DNA synthesis is based on this process. We call metabolism all the chemical reactions producing energy, which can work in two ways. The first one is anabolism, which is a bond making process and needs an energy input (ATP). The second one is catabolism, which is a bond breaking process and releases the necessary energy to ATP. These reactions are catalyzed by enzymes, which are particular proteins contributing to these reactions alone or with cofactors involved. Enzymes are classified according to their functions.
All the reactions are either catabolic or anabolic. Respiration, for example, is a catabolic reaction, where sugar and fats are broken down to produce energy.

### DIY Bio experience

During the course we set up a lab and performed three experiments: we grew bacteria in a colony, used stoichiometry to calculate the amount of carbonated calcium in an eggshell and prepared the environment to grow spirulina.
For the first experiment, we worked in a group to create four different media to grow our bacteria on using materials mostly taken from the kitchen, such as Marmite and salt. The medium is used to grow bacteria by cultivation: it gives the necessary nutrition only to the target bacteria, starving at the same time all the others which could contaminate the colony.
To grow **Vibrio Fischeri**, a bioluminescent bacteria, we:

- Created a Lysogeny Broth (LB) medium using salt (30 gr), glycerol (1gr), peptone (10 gr), marmite (7 gr), Bovril (2 gr), Agar (11 gr)
- Mixed the ingredients with water creating a gelatine
- Sterilized the glass bottle containing it by steaming in a pressure cooker for 15 minutes
- Filled sterilized Petri dishes with the liquid

After cooling the liquid medium to gelatine, we:

- Inoculated it with Vibrio Fisheri
- Let the bacteria grow for 24 hours at room temperature

![]({{site.baseurl}}/Experiment.png)

*Medium creation and grown Vibrio Fisheri colony in the dark*

### Experimenting with DIY Bio

Bacteria live all around us. They can be found in every surface we touch as well as in the pipes that bring water to our houses, on trees and in the streets. My hypothesis for a research would be that **Anthropocene and the increasing urbanization are impacting on the composition of bacteria colonies living in our cities** and, consequently, on ourselves.

**Methodology**

Compare two cities, one highly urbanized, one in the process of becoming so, by:

- Defining signals of urbanization in a city (for example increasing pollution, the composition of water, illnesses)
- Defining a set of specific city features impacted by increasing urbanization (for example water pipes, trees, public transportation)
- Mapping the bacteria living on those features in both cities, studying their roles and behaviour
- Defining a set of bacterias to compare between the two according to roles and behaviours
- Comparing their presence

A further study in the same research would be to discover if and how the differences in the two cities’ bacterias impact on our living by looking at our gut bacteria.

### Resources

- [Hackteria](https://www.hackteria.org/)
- [BioCurious](http://biocurious.org/)
- [BioPunk Manifesto](https://maradydd.livejournal.com/496085.html)
- [Waag Society](https://waag.org/en)
- [VOX: Trump doesn’t have a science adviser. This slime mold is available](https://www.vox.com/science-and-health/2018/3/6/17072380/slime-mold-intelligence-hampshire-college)
- [Wired:SLIME MOLD GROWS NETWORK JUST LIKE TOKYO RAIL SYSTEM](https://www.wired.com/2010/01/slime-mold-grows-network-just-like-tokyo-rail-system/)
