---
layout: page
title:   
permalink: /project/
---

![]({{site.baseurl}}/images/illnesses.png)

**Doctors of the future is an intervention exploring how the urban context we live in impacts on our health and well-being. The aim of the intervention is to understand and improve our life conditions by exploiting technology, intervening on our context, meaning the connection we share with our ecosystem.**

<iframe src="https://player.vimeo.com/video/330339920" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


By 2050, 70% of the world population will live in urban environments.Every city will have a public office of the Urban Doctor, in charge of monitoring the urban interconnections influencing our health and wellbeing.
In the Office of the Urban Doctor doctors, designers and data scientists will work closely with artificial intelligence, monitoring patterns of urban interconnection, preventing and diagnosing systemic illnesses.

<iframe src="https://player.vimeo.com/video/344143454" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

How do we get there?

<iframe style="border: 1px solid #777;" src="https://indd.adobe.com/embed/f396fa58-afed-46bc-a6a7-ded15c8a165a?startpage=1&allowFullscreen=true" width="525px" height="371px" frameborder="0" allowfullscreen=""></iframe>

More on [www.doctorsofthefuture.eu](www.doctorsofthefuture.eu)

## Doctors of the future

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQdY5RZhyHnPAag8D-pqXqLE56N5y28bQQxLV4I41-MGMYPrergYVJZ8CsRoZUeJKz5s2wIGaz2hYnY/embed?start=false&loop=false&delayms=30000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

Cities are complex systems whose infrastructural, cultural, economic and social components are strongly interrelated and therefore difficult to understand in isolation.

The interrelation of the urban dynamic makes it necessary to approach the study of its impact on people from a systemic perspective, one that takes into consideration all the environmental and social factors influencing of health and wellbeing.


**Exposures: a dynamic approach to systemic health**

The urban exposome defines the entirety of exposures affecting us throughout our lives. Some examples:
 - The air we breathe
 - The electromagnetic waves moving around us
 - The social interaction we share with each other.


**Doctors of the future will tackle collective urban health issues**

Emergent urban health and wellbeing threats are systemic and interconnected. Some examples:
-  Pollution causes 1 in 9 deaths
- Lack of physical activity is the fourth risk factor of death worlwide

At the same time, doctors are increasingly hybrid professionals. Can we harness the power of technologies to look at emergent health threats from a systemic perspective?

**Experimenting with the exposome**
To understand the relation between urban exposures and our health and wellbeing, I am planning to sense and gather data on our habits and exposures inside the IAAC building

![]({{site.baseurl}}/images/posters.png)


**Next steps**
- Which illnesses will the doctors of the future diagnose to the context we live in?
- Can we imagine the tools they will use to heal our systems?


## How did the project started?
How much the urban ecosystems influence our health and wellbeing? How many information could we exchange in a city about the personal and collective health and wellbeing of the citizens? Could we design a new discourse around it, one that allow for the citizens to experience a meaningful connection with their ecosystems?

How today and in future self-sufficient cities, citizens can participate and own their health and wellbeing information? It is possible to close the existing gap between citizens and the ecosystem they live in?


## Evolution of my research

[3nd term finals](https://mdef.gitlab.io/2018/silvia.ferrari/three/final/)

[2nd term mid review](https://mdef.gitlab.io/2018/silvia.ferrari/two/midtermreview/)

[End of 2nd term](https://mdef.gitlab.io/2018/silvia.ferrari/two/studio/)

[End of 1st term](https://mdef.gitlab.io/2018/silvia.ferrari/_one/design-dialogues/)

## Bibliography
[What I've been reading](https://mdef.gitlab.io/2018/silvia.ferrari/readings/)
