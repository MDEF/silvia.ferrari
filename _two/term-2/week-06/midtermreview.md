---
title: Midterm review
period: 5 March 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

## Doctors of the future

<iframe style="border:none;width:680px;height:510px;" src="//e.issuu.com/embed.html#35847251/68506307" allowfullscreen></iframe>

**Does the urban environment makes us ill?**
Cities are the ecosystem we live in: how the urban context influences our health and wellbeing as individuals and collectivity?

**How will doctors of the future tackle collective urban health issues?**
Emergent urban health and wellbeing threats are systemic and interconnected. At the same time, doctors are increasingly hybrid professionals. Can we harness the power of technologies to look at emergent health threats from a systemic perspective?

**Can we imagine tools to cure our context?**
I will answer this question through an experiment on urban anxiety



## Midterm reflection
In the first part of the second term I translated my idea for a final intervention into a more defined project. The process involved a lot of struggle, a kind of good, constant internal questioning that I learned to deal with. At the beginning I tried to avoid it, but as the weeks passed I noticed that I was growing dissatisfied and less inclined to work on the final project. I still really liked my idea, but somehow it didn't fit anymore with how I was feeling. Acknowledging it became a turning point in my research, and of all the things learned in the past months, **the most important lesson was this one: in order to keep being involved with my project, to be motivated and to like it, I have to accept that my idea is not a fixed one but will be constantly transformed and inspired by the new knowledge that I acquire in the meantime.**
I realized that I had three main issues with my idea, as I left it at the end of 2018:

- **An awareness intervention**: the original concept of my idea was based on acknowledging that we are surrounded by information about our health and wellbeing that we still cannot read because we lack the interfaces to do so. Somehow, this led me to imagining my project as one aimed at raising awareness on this information. Through my struggling I discovered that awareness is not a sufficient goal to motivate me. I am not convinced that manifesting previously unknown mechanisms is enough to make a change, I am more interested in understanding what is the next step: is it necessary for everybody to be aware? Can I imagine an intervention that goes beyond that?

- **A community based project**: my idea is that it is particularly relevant to read and understand the information about health and wellbeing in the urban environment, because is the one we live in, one in which we oddly feel displaced all the time. Because this is a very interesting point for me, I've been driven to think that a possible intervention had to deal with communities in the city. When I imagined it, I was again somehow driven to imagine something that I discovered I am not interested in: community engagement. Acknowledging this has been difficult, because a huge part of my past activism had to do with it, but it was very liberating as well. Although I understand the power of it, I would prefer to focus my project on something different.

- **Thinking vs making**: throughout the term, I found myself more willing to engage with practical activities than formal reflection. It has been a slow change, but a very important one for me. Everything started with the first class of material driven design, that we had in Valldaura. I wrote about it [here](https://mdef.gitlab.io/silvia.ferrari/two/material-driven-design/). The Fab Academy followed that, and I found myself more motivated to work on its assignments than on everything else. I find myself more driven toward making now, I would like to focus on that and integrate this part of my studies into the final project as soon as possible. At the end of last term I doubted that my final intervention could be related to Fab Academy, but now it has become a prerogative for me.

## Feedbacks and research

Incorporating these new findings into my idea has been a hard job and it is still a work in progress. I researched a lot (the complete list of my readings is [here](https://mdef.gitlab.io/silvia.ferrari/readings/) ), but I have been also helped in the process through the discussions I had with most of the faculty, Indy Johar and Mara Balestrini and by participating in a Nesta workshop hosted by IAAC.

MDEF faculty and Mara Balestrini, who I met separately, told me over and over that I need to narrow down my idea by:

- **Defining an item**: Mariana made me asking myself which is the aspect I am more interested in? Is it making health and wellbeing collective data understandable? Is it finding a way to act on these data as a community/collectivity? Mara Balestrini pointed out that I should stop being so general and pushed me to find a more specific area of intervention. Which item of health I am interested in? Is it air pollution? Anxiety? Sound pollution?

- **Defining a research methodology**: which tools should I use to investigate my field of interest? Oscar provided us with some useful ideas during one of our Design Studio class we held at Elisava, and Jonathan also gave me useful insights on how architecture has looked into my issue, especially through the definition of [Sick building syndrome] (https://it.wikipedia.org/wiki/Sick_building_syndrome) and situationism. Moreover, Mara Balestrini provided me with a lot of references to look at, academics who researched into collective health by using innovative tools to map the cities.

- **Asking myself why**: why do I believe that the issue I am researching on is a relevant one? Why do I believe it is important to act on urban collective health? I unconsciously knew the answers, but making with Tomas the exercise of better defining *the context of my intervention* provided me with direction and grounding.

The **Nesta workshop** had the aim of imagining possible future scenarios in some relevant fields, among which Healthcare. The participants in my group had very interesting background, from Salus.coop to the Mobile World Foundation, and talking with them proved to be very useful for my research. **I learned a lot about how the healthcare system is structured, who are the relevant stakeholders and which are the most pressing issues regarding collective health according to people with a background in the field**. Our discussion into the future of healthcare soon focused on public awareness and citizen science. The scenario we imagined was, in the end, one where citizen science contributes to collective healthcare through hubs, laboratories where citizens, public health and private research could meet and engage together in finding new solutions for very specific problems such as diabetes and respiratory illnesses. These hubs were imagined also as places where communities could be educated on the most relevant issues in collective healthcare. My dissatisfaction with the final solution made me acknowledge that awareness/education and community engagement are not my goal now, but at the same time **the knowledge I could gather from the other participants proved to be very useful for my idea.**

Lastly, the meeting we had with **Indy Johar** was really useful for me because it provided my research with **a framing that I like very much.** His suggestion was to look at my idea from the point of view of the doctors of the future. Medicine is being influenced so much by innovation and new technology and the threats we are facing now require a new approach to the profession. Indy agreed with me: **the urban context is going to be increasingly relevant for healthcare**. Cities are the place we live and the environment affects us in so many ways that we need a way to look at our wellbeing from a collective perspective. He asked me to look at the issue by asking myself **how will doctors of the future will be able to heal the context**?

The result of my journey through the first part of the term could be found on my [final project's page](https://mdef.gitlab.io/silvia.ferrari/project/)

## Next steps
- Is it possible to imagine a city sick syndrome taking inspiration from [Sick building syndrome] (https://it.wikipedia.org/wiki/Sick_building_syndrome)?

- Developing a methodology to map urban anxiety: personal perspective through self tracking/derive vs collective perspective through quantified communities

- Mapping stakeholders in Barcelona

- Which tools can we imagine for Doctors of the future? Providing an answer through Fab Academy
