---
title: Design studio
period: 9 April 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---
In the second part of the second term, I worked on improving my idea for the final project. I was left a bit unsatisfied and still unsure of how to carry it on after the midterm review. As I wrote in the [last submission](https://mdef.gitlab.io/silvia.ferrari/two/midtermreview/) on the topic, I learned that in order to like what I do, I have to accept that it will constantly change.  So I tried to let my thoughts go, taking their own direction without worrying to be lost. I was helped in my journey by two events. First of all, as I wrote in the [Atlas of weak signal submission](https://mdef.gitlab.io/silvia.ferrari/two/atlas/), that class helped me finding narratives for my project, new perspectives from where I could look at it and find it even more interesting and engaging than before. Secondly, during my research on the Doctors of the future's concept, I bumped into the medical term **exposome** - the total amount of external exposures we experience throughout our life that affect our health- which perfectly defines the object of my project. Before it, I had a set of keywords that I used to look for material on my topic (upstream health, social determinants of health), but finding a way to give a name to the object of my research was a huge step forward for me. Since then, I have been able to find information way more easily. Also, giving names to things proves they exist: through my discovery, I felt reassured over my general idea.

My presentation for the design studio aimed at creating a journey into my idea - which I reckon is not immediate to understand - starting from the context and then diving into the concept of exposome and the experiment I aim to carry in the next term.

## The context

<iframe src="https://player.vimeo.com/video/330339920" width="640" height="480" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


Since my project imagines a new role for the doctors in a city of the future, putting the context together was a relevant part of the work I had to do.
Through the Atlas' classes and other research, I was able to place my project at the intersection of events that are occurring, weak and small signals that together could bring a massive change in the near future.
The effects of climate change in urban environments, the rise of AI in medicine and its consequences, the studies showing how detachment and loneliness are increasingly a global health issue and the possible consequence of massive data exploitation were among the most relevant topic for me.

As I imagine it, in the near future there will be a new profession for doctors, aimed at healing the context people live in (instead of single persons). These doctors will be hybrid professionals that along with data scientists and designers will monitor the urban exposome looking for interconnections - the way external exposures affect our individual and collective health, diagnosing the context's illnesses and planning how to heal it.

## The exposome

There is increased attention in academic research towards what determines our health. Experiments in both [medicine](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2698635) and [computer science]({{site.baseurl}}/documents/Social sensing for epidemiological behavior change.pdf) have shown how the external context could affect our wellbeing. We are interconnected with our environment and this interconnection affects us in many ways. Medicine is more and more studying this area: **upstream medicine** studies the remote causes of illnesses. The concept of **exposome** goes further and aims at understanding how the exposures we experience have an influence on our health. The concept of exposome is studied mostly as a medical subject, and for this reason, the research done in the field is very specific and deals with our health at the level of genes and biomarkers. There is however an emergent field of research aimed at understanding our interconnection with the external environment in a more holistic way, taking into account both environmental and social exposures to explain how they affect both our health and wellbeing.

Drawing on that research, I created an **urban exposome framework** to evaluate both the environmental and social exposures. In my idea, that's the framework the doctors of the future would use to monitor the environment. For the presentation, I created a booklet which explains in detail how the urban exposome framework was created.


<div data-configid="35847251/69148941" style="width:100%; height:600px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>


## The experiment

The last part of my presentation explained the experiment that I aim to carry on during the next term. In my idea, **the urban doctors of the future will have already defined a set of urban illnesses, defined by the sum of interconnected manifestation that could affect our health and wellbeing. These illnesses would help doctors to diagnose a sick city.**

To define these possible illnesses however, it is necessary to understand how the exposures could be interconnected, and how we are affected by this interconnection.
For this reason, I decided to use the parts of the IAAC building MDEF uses the most as an example of an urban environment. My plan is to monitor both environmental and social exposures through a mix of sensors and qualitative data. I will use my classmates as a population and will monitor how the exposures affect them. From the mix of data I will gather, I aim to define a set of interconnected sickness, which will be the subject of the doctors of the future's diagnosis.

The first step of the experiment was to create a catalogue of possible exposures in my selected environment: I worked on this at the same time I was creating the exposome framework. Doing both jobs together helped me defining a set of indicators and possible ways to monitor them inside the building. For the presentation, I created a booklet explaining this process, imagining to play the part of the Urban Doctor's Office

<div data-configid="35847251/69148975" style="width:100%; height:600px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>

To show how I created the catalogue of possible exposures for the final presentation, I used medical exams as a metaphor. I first walked around the building **listing all the exposures I could feel on me.**

![]({{site.baseurl}}/xrayposterblack.png)

Secondly, after having defined the indicators of my urban exposome, I decided to visualize the same exposures in space, to see where they overlapped and which parts of the IAAC building were more relevant to study for my experiment

![]({{site.baseurl}}/elettrocardiogramma.png)

Lastly, I defined which tools I could use to monitor the selected indicators.

![]({{site.baseurl}}/Recipe.jpg)


## Feedbacks received

- **Narrative**: I was able to talk to almost all the people who came to see our presentation. My pitch improved with every person I talked to, but I know that without me talking something would have gone missing in the presentation. I received very positive feedback on the video, which defined the context for the intervention, but others pointed out that **I need to simplify my narrative to make it possible for everybody to understand the process of my idea**. I plan to prepare in the first week of the next term a presentation to use during my future pitches, following the same structure I used to talk during the design studio. Also, it was suggested that I should focus more on how the urban exposome index was created, stating more clearly that I've been building the framework and experimenting at the same time. I also decided not to talk about the specific research that I did (readings, interviews with doctors), but it was suggested to put it into the following presentations.

- **Speculation**: It was suggested to place my speculation about the doctors of the future in a specific year - 2050 - and start my narrative from there. I was told I could play with the doctor's metaphor even further than I did, using for example well known current medical tv shows and bringing them into my speculation. What would House do with my idea in 2050? What about Grey's Anatomy?

- **Data framework**: I am very interested in possible frameworks that could ensure personal ownership of the data produced. When I first came up with the idea of my project, data sharing was one of the topics I was interested the most. I did not focus on it for this presentation, but I was asked how I envision the use and exploitation of data in the context of my doctors of the future's idea. In the future, I plan to reintroduce the topic into my presentation.

- **Experiment**: it was pointed out that, carrying my experiment, I should prioritize efficiency of the results. For this reason, I should be paying attention at asking **scientifically correct qualitative questions** and think about modifying my original idea - to create a chatbot to asl them - replacing it with a link for a google form.
I was also given useful advice on how to ask good qualitative questions. First of all, it is important to put people in the context of the questions - without being too general about it- and require polarized answers. I should target well what I am trying to cover, directing people towards small, specific answers that are subjective and personal rather than evaluating. Before starting the experiment, I should test the questions I come up with different people, to ensure the spectrum of the questions is the same for everybody.

- **Resources**: additional resources to draw inspiration from were suggested to me, among them [signature sets to find life in new planets](https://exoplanets.nasa.gov/the-search-for-life/life-signs/) and Domestic Data Streamers Instagram account, where they frequently post qualitative and personal questions they ask in their office. Domestic Data Streamers also suggested replicating a project they did in a hospital, where cancer patients were asked to map their feelings towards the care system on a map of the hospital. For example, which room made them more anxious?

- **Final presentation**: it was pointed out to me that **I should start thinking now about how to present my project in June**. A very nice idea suggested to me was to create, among other things, a small documentary. For this reason, I plan to start gathering material at the beginning of the next term and create footage of the experiment while doing it.

- **Further action**: I was advised to **start thinking at further actions on my project**. What could happen after I have done my experiment and defined a set of urban illnesses? In research terms, it was suggested to me to organize a dialogue about my topic between current doctors and medical students, to see how they respond to my speculation. In terms of the future of the project, I was given some suggestions on possible fundings options. Also, it was pointed out I could publish my framework and methodology, make it open source and scale it to make a platform to let other people try the same experiments, maybe with different sensors.

## Readings

[Here](https://mdef.gitlab.io/silvia.ferrari/readings/) I collect all the readings I'm doing on the Doctors of the future

## Next steps

- The experiment: through the experiment, I will define the interconnected pathologies that doctors of the future will diagnose

- Throughout the experiment, I will record footage and produce a video to showcase it to the final presentation

- I also plan to develop a sensor, a prototype of the sensors that doctors of the future will use. The aim of it is to create a way to sense environmental as well as social exposures

![]({{site.baseurl}}/plan.png)
