---
title: Introduction to shrimp bioplastic
period: 28 January - 21 February 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

## 1. Introduction

The aim of our material design project was to create a product using a new biodegradable material, created by us without using chemicals through an abundant source available in the city.
The project required to "live with the material", meaning getting to know its properties by experiencing it in as many possible ways to discover possible uses and, in the end, a product.

I really appreciated the idea behind the project, as [I explained in my post about material design](https://mdef.gitlab.io/silvia.ferrari/envisioning/material-driven-design/). The recipes to replicate my work are in the last paragraph of this long post.

## 2. Why shrimp shells

My first idea was to work with fallen plane tree leaves. Plane trees are such an interesting element in the urban context. Their bark is able to retain pollutant particles. This, along with their endurance in an urban environment, is the reason why they are so abundant in European cities.
Interestingly, the municipality of Barcelona decided a few years ago to reduce the amount of plane trees in the city, switching towards a policy of diversification of the urban green. One of the reason behind the decision was that plane trees are a massive source of allergens, impacting on the population's health.
While in the process of researching plane trees, I had another idea: to work with shrimp shells. I was a bit unsure about it, mainly because I imagined that sourcing the material would have been difficult. I talked with Mette about it and she was very open to the idea. As I discovered, shrimp shell is one of the most interesting materials to work with, thanks to **chitin**.

The shrimp waste contains several bioactive compounds such as chitin, pigments, amino acids, and fatty acids. These bioactive compounds have a wide range of applications including medical, therapies, cosmetics, paper, pulp and textile industries, biotechnology, and food applications. **Chitin** is a polysaccharide sourced from exoskeletons: this means that it is abundant in the shells of crustaceans and insects. It is the **second most abundant biopolymer** on Earth, after cellulose.

Experimentations around the creation of a bioplastic from chitin started a few years ago with **Shrilk,** a fully degradable bioplastic developed at Harvard made with chitosan (a product of deproteinized chitin) and silk. Shrilk was tested to be used as a plastic substitute and also for creating implantable foams, films and scaffolds for surgical closure, wound healing, tissue engineering, and regenerative medicine applications .

Shrilk was discovered to be very expensive to source and produce, but chitosan alone - without silk- became the object of various experiments at the intersection of science and design. The most famous experiment around chitosan and its possible application as a bioplastic is the one performed at [MIT's Mediated Matter Lab](https://www.behance.net/gallery/65182471/Water-Based-Digital-Fabrication): scientists created a wing-like multifunctional structure using a custom-made 3-d robotic printer, which extruded chitosan in different concentrations.
The most interesting property of chitosan's bioplastic is that **it is not water resistant**. Although the process of degradation is quite long, products made with this plastic eventually dissolve in water. Another interesting aspects of the material is that, even though crustaceans contain quite powerful allergens, the bioplastic is hypoallergenic.

As I discovered, this amber-coloured, translucent bioplastic is also very beautiful.

![]({{site.baseurl}}/experiments.png)

## 3. Sourcing the material

The first step into the project was to source the material. Shrimp shells are supposedly abundant in Barcelona because of tourism, the amount of restaurants and - I thought - paella.
As me and Kat (who worked at the project with me) discovered, it is not that easy to collect the shells. The first batch was a gift from our classmates, who collected it from a stand at the Boqueria.
I then started asking around in restaurants and markets in my area - El Born - but people were reluctant to help me in my very specific task, which was to collect **raw** shells.

So instead of asking to the final recipients of shrimps, we started researching along the production line. Where does restaurants source shrimps? Where does fish stalls in the markets get them?
We discovered that in the suburbs of the city there is a place called [Mercabarna](https://www.mercabarna.es/), the wolesale market of the markets of the city, where most of the food consumed in Barcelona comes from. Among many others, Mercabarna has a fish market which is open **all nights (apart from Saturday), from midnight to six in the morning**.

![]({{site.baseurl}}/mercabarna.png)

*Exploring Mercabarna*

The trip to the market was a very interesting experience: we were the only girls, the only visitors that didn't want to buy any fish. It was an important step, it allowed to:
- **Find a reliable source of shrimp shells** from market stalls wanting to get rid of shrimp's heads
- **Reflect on the concept of circularity**: because Barcelona is a maritime city and because paella is such a common dish, I somehow implied that shrimps were fished locally. I had the opportunity to ask to the people working at the wholesale market, and I discovered that most of the crustaceans we come from the Western coats of northern and central Africa, specifically Mauritania, Gambia and Senegal. Intensive shrimp fishery brings along a wide variety of issues, among which the most important are [trawling and its bycatch ](https://www.worldwildlife.org/industries/tropical-shrimp) and [overexploitation](http://firms.fao.org/firms/resource/10148/en)



## 6. Bibliography

**Chitosan bipolastic**
- [The Shellworks](https://www.theshellworks.com/)
- [Plastic made from shrimp shells](https://www.popsci.com/article/science/plastic-made-shrimp-shells)
- [Shrilk Biodegradable Plastic](https://wyss.harvard.edu/technology/chitosan-bioplastic/)
- [neri oxman and MIT develop digitally produced water-based renewable material](https://www.designboom.com/technology/neri-oxman-mit-mediated-matter-water-based-digital-fabrication-05-14-2018/)
- [MIT Mediated Matter](https://www.behance.net/gallery/65182471/Water-Based-Digital-Fabrication)
- [Water - Based Robotic Fabrication: Large - Scale Additive Manufacturing of Functionally Graded Hydrogel Composites via Multichamber Extrusion](https://core.ac.uk/download/pdf/83231644.pdf)
