---
title: An introduction to material driven design
period: 20-25 January 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

The times we live in require us to shift our perspective on production and consumption, aiming at sustainability and circularity.
Sustainability is achieved when an action has a positive impact
 - for the planet - meaning that is bearable
 - for the people - meaning that the action is equitable
 - for the profits - meaning that it is viable and doable from a financial standpoint

So, to be pursued, **sustainability has to be supported by a system that values it**, putting it at the core of innovation. This requires a change of perspective in the design research and processes.**Knowing** how our system work and how a circular economy should be is different from actually **understanding** it: designers need to regain control over the production system to understand it and being able to make it more sustainable.

This is the goal behind our course on material driven design: its aim is to put materiality into the design process again, because a better understanding of the materials used in production would allow regaining control over the products we create, redesigning the whole process to be sustainable and circular. I believe this reflection is particularly interesting and relevant in the environment we are currently immersed into: FabLabs are innovative but they do not drive sustainability, they produce lots of waste and don't take into account enough the origin of the materials used.

From an educational point of view,  we lack knowledge about materials. Also, we are not taught how to experience closeness to the materials we use, and in this way we are **detached** from them and from all the consequences that stem from the choices we make in designing with harmful materials.
This distance that we put between us and the materials that we use to design is further increased by the growth in the **options** we have regarding materials. In the last century the amount of them has massively increased, thanks to the research into chemistry and the development of plastic. The need for a more sustainable approach to production and consumption, one that also takes into account the waste we produce, requires us to reconnect with the materials and the natural realm.

I think that the material driven approach to design is really cool and brave: it requires to put the most difficult circular step to solve at the beginning of a design reflection, instead of leaving it unsolved, at the end of the process.

To reconnect and learn about materials, the best way is to experience them and learn through **embodied cognition**. Learning is not only shaped by our brain, but our senses and bodies play a part in it. This duality is expressed in the difference between **online cognition**, which is conscious, mind based and require awareness, and **offline cognition**, which is unconscious, body based and doesn't require our focus. Studying on a book, coming to class and writing this post are online actions. The kind of relationship we have to pursue with materials has to be unconscious and offline, and falls in the second category.

Offline learning empowers a different expertise, one that is based on instinct and physical knowledge. I discovered it during the first exercise we had to perform: carve a spoon from a block of wood during our day at Valldaura

![]({{site.baseurl}}/wood.JPG)

As soon as i got my carving knife and started cutting, I fell in a very particular state of mind, one in which I was half concentrated and half distracted from the carving, but that at the same time allowed me to take care of the details of the work I was doing with a very intense focus. Remembering it, the carving of my spoon takes the form of a story, with some hard challenges - such as when I discovered three different termite nests- and very good moments.

![]({{site.baseurl}}/spoon.jpg)

I have been educated in a Montessori school, and I credit it for the ease and fun with which I approached this task and the rest of the course as well. The Montessori methodology puts experience at the core of a child's development: everything I have been learning from kindergarden to middle school I learned by doing, almost never opening a book.

The exercise was a useful introduction to the assignment we have to complete for the class, which is to source an abundant material in the city to develop a new material that has to be completely biodegradable. To understand our material properties and possible applications, it is necessary for us to replicate the same state of mind and kind of learning we applied to the spoon.
