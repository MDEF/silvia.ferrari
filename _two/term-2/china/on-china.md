---
title: On China
period: 7-14 January 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---

In the first weeks of January 2019, MDEF went to visit Shenzhen. To prepare myself for the trip, I spent the Christmas holidays reading about artificial intelligence, the Chinese economy and the power struggle between the US and China. My journey-before-the-journey was very interesting. I noticed very soon that the emergence of China as a tech superpower is often framed into an adversarial discourse, where the west and China are opponents in a race towards the world's domination, and it seems like there's no space for cooperation or learning.
As I wrote before, I believe dichotomies are not anymore a valid instrument to evaluate present and future systems, and thinking outside of our cultural knowledge box is necessary to understand at least some of the things that are happening around us. Doing otherwise, especially when talking about geopolitics, is blind and dangerous. I tried to approach the China trip with this critical perspective, making comparisons between what I'm used to and what is new to me but also trying to distance myself from the westernized narrative on China's rising power. My teammates - with whom during the trip I had to reflect on AI and cognitive systems - shared the same perspective. As a result of the trip, we produced a presentation discussing our main takeaways and a video, introducing the topic of AI in China.

<iframe src="https://player.vimeo.com/video/312961246" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/312961246">Shenzhen research trip - MDEF Ai group.</a> from <a href="https://vimeo.com/user92093114">G&aacute;bor L&aacute;szlo M&aacute;ndoki</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## The mindset
China's ability to develop new ideas into tangible things is impressive.
This flexibility and speed of execution  are applied to everything, from the infrastructures to the development of new technologies , and are entirely unknown to us. How is it possible to be so fast at deploying solutions? I found a partial explanation for this in Kai Fu Lee's book's [AI Superpowers: China, Silicon Valley and the new world order](https://aisuperpowers.com/): "China's startup culture is the yin to Silicon Valley's yang: instead of being mission-driven, Chinese companies are first and foremost market-driven". David Li confirmed this attitude to us. When questioned about how the Chinese makers approach the prototyping stage of production, he answered: "products are put on Taobao and if they work we make more". It seems to me that the Chinese mindset is completely opposite to ours: while we're used to researching a lot and develop one solution, their approach is to minimize everything that comes before the deployment, using the market as the most important test.
This lightness makes it possible for everybody to be faster and it also probably makes the production stage cheaper than we are used to. Also, because there's virtually space for everybody, it seems to me that cooperation is valued more than competition in the production system: we've been explained how Shenzhen's model of innovation take advantage of unparalleled access to open technology and proximity to production.

It seems to me that  the whole country  is able to act fast: governmental plans are deployed every five years, and the goals are always met. To achieve these results,I believe there must be a tight relationship between the government and the corporate world, which act together towards common goals. It's a completely different social contract from the one we're used to, one that allows for the population, the market and the government to work together to achieve common goals in a trusting relationship.

## Data
From what we have experienced, the trust in the system is high in China. It would be interesting to have a Chinese point of view on this topic, but given how difficult it is we are left with our observations alone. Even if the country's plan to develop powerful artificial intelligence is manifest, it seems that nobody worries about the amount of data that is shared on WeChat and other apps, and the CCTV system doesn't seem to bother anyone. Sharing is widespread: I was amazed to see the multitude of services offered in China, from power banks to umbrellas in the metro stations. In the context of a developing AI, data become a commodity. Because of this, we Europeans share the belief that data sharing can't be trusted. Companies will profit from us in ways that we can't imagine, governments will use it to control our behaviours. We perceive these actors to have different goals from us, and we value privacy over sharing. This doesn't seem to happen in China, I think because the whole country is able to be united towards common targets. This attitude made me reflect on the system I live in. We pretend to live in a system where data are protected, but is it really so? And, even more important, why do we value protection so much? It seems to me we live in a place where the capitalistic market has become such a powerful entity -almost equal to our political institutions - that we feel the need to be protected by it. This is maybe why the west discusses China's emergent power in terms of data colonization. According to Yuval Harari, in the near future: "those who control the data could eventually reshape not only the world's economic and political future but also the future of life itself. "

## Innovation
How does the fast, product-oriented Chinese mindset adapt to innovation? From what we have seen, to innovate in China means to improve existing ideas more than disrupting the system, which is a more western perspective.
How will this impact the developing of artificial intelligence? Will it be more industry oriented or user-oriented? And how will future generations grow up, with such a close relationship to smart devices and AI?
We would have liked to have a Chinese perspective on our reflection, but anyway I think we left with a more insightful perception of the country and we had the opportunity to reflect also on the system we live in


## Readings
- [AI Superpowers: China, Silicon Valley and the new world order](https://aisuperpowers.com/)
- [DigitalPolitik](https://medium.com/digitalpolitik/digitalpolitik-2-b7ae56f0a495)
- [Who Will Win the Race for AI?](https://foreignpolicy.com/gt-essay/who-will-win-the-race-for-ai-united-states-china-data/)
- [Why we need to be wary of narratives of economic catastrophe](https://aeon.co/ideas/why-we-need-to-be-wary-of-narratives-of-economic-catastrophe)
- [Clash of the US and Chinese tech giants in Africa](https://www.ft.com/content/ff7941a0-b02d-11e8-99ca-68cf89602132)
- [How the tech industry got caught in the Trump-China trade crossfire](https://www.wired.co.uk/article/china-usa-trump-trade-war-tech-companies)
- [A New Old Threat](https://www.cfr.org/report/threat-chinese-espionage)
- [The Hacked World Order: How Nations Fight, Trade, Maneuver, and Manipulate in the Digital Age](https://www.amazon.com/Hacked-World-Order-Maneuver-Manipulate/dp/1469065894)
