---
title:  Experiments and recipes of a shrimp bioplastic
period: 21 February - 7 March 2019
date: 2019-01-24 12:00:00
term: 2
published: True
---

## 1. Experiment

Once we found the source of material, we focused on the experiment. The goal was double: to extract chitosan from the shells and discover other possible material applications.

Chitosan is so widely use in medical research that it's quite easy to find  scientific references & articles online on material experiments. It was more difficult to find "in-between" references, meaning some examples of chitosan/shrimps experiments more focused towards design and the properties of the material. Anyway, reading academic papers we found out how chitosan is extracted from shrimps through two different methodologies:

As we noticed, there is apparently no way of extracting chitosan from crustacean shells without using caustic soda, a powerful chemical that we were willing to avoid. We were able to talk about this issue with some amazing students from RCA, collectively called [The shellworks](https://www.theshellworks.com/). The aim of their project, the only one we found at the intersection of design and research, is to design and produce specific machines to handle the chitosan bioplastic's properties and create objects.
The shellworks confirmed to us that there is no way of extracting chitosan without chemicals. After a talk with Mette we decided to skip the extraction step, buy chitosan powder online and focus our efforts on the more practical side of the experimentation, meaning the creation of final products from chitosan.
Nonetheless, we continued experimenting on possible application of shrimp shells outside of the chitosan scope.

![]({{site.baseurl}}/process.png)

*How to get from shrimp shells to chitosan**

## 2. Reflections on the process

Focusing on chitosan allowed us to focus more on the final material and its possible applications, but at the same way it distanced us from the original source, shrimps. It was a pity but we had a short amount of time to complete the process.

Experimenting with chitosan plastic has been fun, mainly because it is such a strange material to handle.
Initially the plastic is a gel, that then has to solidify through water evaporation. This means that whatever final product one has in mind, it is necessary to take the shrinking process into account when preparing molds for the bioplastic.
At the same time, working with a gel can be challenging: because it's such a watery form, it is difficult to create vertical structures such as cups. The gel will always tend to go back to the flattest possible shape.

These properties of the material required a lot of thinking about the molds and how to create shapes. The only reference in mind for me was the work done by The shellworks. To create a cup for example, they created a machine that heats a cup mold and then dips it into chitosan bioplastic, letting it solidify immediately. The process allows to create layers until the cup is sturdy enough to be ready.

I found that the best home-made solution is to let the gel dry initially as a sheet, and then proceed to shape it in the lattest part of the drying process, where the bioplastic looks and feels more like a resin.
I tried to create cups this way, and this is my best result so far. However, since I still have a lot of chitosan powder, I will continue my experimentation. Currently I am working on a 3d printed double mold that will hopefully allow me to put the gel vertically right from the beginning.


## 3. Recipes

![]({{site.baseurl}}/demineralized.png)

![]({{site.baseurl}}/plastic.png)


## 4. Bibliography

**Chitosan preparation**
- [Efficient use of shrimp waste: present and future trends](https://link.springer.com/article/10.1007/s00253-011-3651-2)
- [Methods for extracting chitin from shrimp shell waste](https://www.ncbi.nlm.nih.gov/pubmed/9754407)
- [A new method for fast chitin extraction from shells of crab, crayfish and shrimp](https://www.ncbi.nlm.nih.gov/pubmed/25835041)
- [Extraction of chitin from prawn shells and conversion to low molecular mass chitosan](https://www.researchgate.net/publication/257103033_Extraction_of_chitin_from_prawn_shells_and_conversion_to_low_molecular_mass_chitosan)
- [Extraction of chitosan and its oligomers from shrimp shell waste, their characterization and antimicrobial effect](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5352841/)
- [Preparation and Characterization of Chitosan Obtained from Shells of Shrimp](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5450547/)

**Chitosan preparation by fermentation**
- [Fermentation of Shrimp Biowaste under Different Salt Concentrations with Amylolytic and Non-Amylolytic Lactobacillus Strains for Chitin Production](https://pdfs.semanticscholar.org/4e93/c9a952a504027c950071925c66d565e49926.pdf)
- [Chitin extraction from shrimp shell waste using Bacillus bacteria](https://www.sciencedirect.com/science/article/pii/S0141813012003509)
