---
title: Applications & Implications
period: 10 - 17 April 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---


## What is the scope of my project?

Doctors of the future is an intervention exploring how the urban context we live in impacts on our health and well-being. The aim of the intervention is to understand and improve our life conditions by exploiting technology, intervening on our context, meaning the connection we share with our ecosystem.

For more information visit the [final project page on my website](https://mdef.gitlab.io/silvia.ferrari/project/)

## What will it do?

Since working at the urban-scale level it's very complicated, I plan on staging a smaller intervention the following weeks.

To understand the relation between urban exposures and our health and wellbeing, I am planning to sense and gather data on our habits and exposures inside the IAAC building.

The plan involves:

- Gathering a mix of environmental data and experiential data over a week time in the Fab Academy Room, in Room 201 and various other locations in the building (where the machines are, the staircase, the vending machines among others)
- The environmental data will be gathered through Smart Citizens Toolkits and I will also fabricate a sensor to gather data on electromagnetic waves
- The experiential data will be gathered through questions scattered across the building following the Domestic Data Streamers methodology
- After data gathering, I will map the data through time looking for interconnection between exposures and our feelings

## Who has done what beforehand?
In terms of sensing environmental exposures the main reference is the Smart Citizen Toolkit. For the experiential questions the main inspiration comes from the Domestic Data Streamers [office questions](https://www.instagram.com/p/BvMb0_TBfa6/?utm_source=ig_web_button_share_sheet)

## What materials and components will be required? Where will they come from? How much will it cost?
Smart Citizen Toolkits, Webcams, Raspberry PI, papers and stickers

## What parts and systems will be made?

- I plan to make an EMF sensor following [this project](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/).
The **bill of materials** is:
1x double sided copper PCB
2x Resistor 10K
1x Resistor 47R
3x Capacitor 1nF = 1000 pF
1x Capacitor 100nf = 10000pF
1x LT5534

- The Smart citizen toolkits will be connected to each other through a network, which I plan to set up

- I also plan to capture the movement in the rooms through a webcam connected to a raspberry PI. I will need it to understand if the rooms are empty or full while capturing data.
The **bill of materials** (probably to expand) is:
2X webcams
2X Raspberry Pi
2X SD cards

- There will be conding involved for the movement monitoring and the overall network setup

## What processes will be used?
PCB design, CNC milling, soldering, coding

## What tasks need to be completed? what is the schedule?
For a complete overview of my project streams and tasks visit its [GANTT](https://docs.google.com/spreadsheets/u/1/d/1wyXbg_4fAPysFopUjC-BPyyIUQwpaE0JATg0CVIiVrQ/edit#gid=0)

## What questions need to be answered? How will it be evaluated?
The experiment will demonstrate if it's possible to gather information on our health and wellbeing through sensing 
