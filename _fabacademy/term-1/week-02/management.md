---
title: Project management
period: 23-30 January 2019
date: 2019-01-29 12:00:00
term: 2
published: true
---

**week assignment: Build a personal site in the class archive describing you and your final project**


## Building a personal website using gitlab
As MDEF student, we went through the process of creating a personal repository using Gitlab at the beginning of the academic year. We have been using it to upload our weekly reflections since then, and the main task for the Fab Academy was to add to our repository a new section dedicated to the Fab Academy weekly assignment

### Creating a repository with Gitlab
1. Sign in/create a new account in Gitlab to create a personal repository

2. To work locally on the repository, generate a SSH key to clone it on your computer, creating a secure connection between the computer and the cloud. To do so:

    * On the computer terminal - I am using [Hyper](https://hyper.is/) - input the commands ``ssh-keygen –t rsa –C “$my_email”``  and then  ``cat ~/.ssh/id_rsa.pub``  to generate your SSH key

    * Authorize the key for the repository by adding it into the Gitlab repository in **User Settings >> SSH Key**


3. Now that the connection is authorized, it is possible to copy the repository on the computer. First choose or create the folder where you want your repository to be:

    * On the terminal: use the commands  ``cd`` and ``ls`` to move inside the computer.

      * ``cd``enters a directory
      * ``ls``lists the content of the directory

    * Once in the chosen directory, use the command ``git clone + SSH key`` to add the Git repository to the directory


4. Now it's possible to work locally with the Gitlab repository. To manage it and add content, I choose to use [Atom](https://atom.io/), which is both a text editor and a managing tool for Gitlab, and allows to create new **branches**, **stage** and **commit** changes directly into the repository.


### Managing the website
The public access to my repository is a website, running on Jekyll. To install it, I followed the tutorials available [on the Jekyll website](https://jekyllrb.com/docs/installation/).

The repository's structure is very simple to manage and edit. In the past term, I organized my weekly reflection into a specific folder. Inside it, each week has a separate folder where I could put my .md files and the images/other files I published with the post as well. The posts were listed into the Reflections page, which automatically ordered them by date - the lattest at the bottom.

The layout code for this page, as well as the whole repository structure and code, were given to us by Ilja, who created it and made it available to all the classmates.

For the FabAcademy, I would like to replicate the same repository structure I used for the reflections, adding also a new tab in the website's header to access it. My process was as following:

I created a new ``_fabacademy`` folder, replicating the same structure as the Reflections one:

  ![]({{site.baseurl}}/folders.png)

  Inside each week folder, I put a **.md** file replicating the same structure as the reflection posts:

  ``---
  title: Project management
  period: 23-30 January 2019
  date: 2019-01-29 12:00:00
  term: 2
  published: false
  ---``


Into the ``_layouts`` folder, I created a new layout for the Fab Academy posts list,``fabacademy.html`` based on the one i used for the reflections:

  ![]({{site.baseurl}}/layout.png)


I added the new custom collection to the ``_config.yml`` file
  ![]({{site.baseurl}}/config.png)


Into the main ``silvia.ferrari`` folder, I created a new ``fabacademy.md`` page, where I would like for all the weekly Fab Academy posts to be listed. The page is blank apart from the YAML code, but it follows the ``fabacademy.html`` layout i have created before.

  `` ---
  layout: fabacademy
  title: Fab Academy
  permalink: /fabacademy/
  ---``

Into the ``_includes`` folder, I modified the ``header.html`` file to add a Fab Academy tab, linking to ``fabacademy.md``.

  ``<li><a href="{{site.baseurl}}/fabacademy" {% if current[1]=='FabAcademy' %}class='active' {% endif %}>Fab Academy</a></li> ``

It took me many attempts to manage the whole process. I relied on my classmates help and then also learned by doing it. I noticed how easy it is to make typing mistakes, for example forgetting to put an underscore, leaving blank spaces or putting the wrong file name into code.

In the end I'm quite happy with the result, but in the future I would like to add new layouts to my website to make the interaction less static.
