---
title: Principles and practice
period: 16-22 January 2019
date: 2019-01-29 12:00:00
term: 2
published: False
---
## The final project - exploratory ideas
I want to spend my fab academy time to learn and deepen aspects of my final project. My general idea is

**Aspects that I would like to deepen into fab Academy/themes**
- interactivity with Data through reaction: i would like my object to be triggered by data or to trigger a reaction in the person playing with it
- exploration of hidden aspects of our life (fabricating food for example)
- i would like the project to embed some aspects of circularity, either through bio materials or ewaste. It would be great to keep the experiment urban
- challenge the definition of traditional human computer interface that was constrained by a computer screen - tangible user interfaces
- connection between different things in the environment

**Which principles I would like to follow**
- Balance between technical and creative side: I would like to, without drowning in technological aspects that could cripple my idea. I would like to avoid Drown in technology: I would like to experiment with the tools and technologies we're going to lear trying to  focus more on creativity than on technicalities, I want to be able to use fully the instruments provided without having to worry too much about size/impossible things to do
- Focus on learning, not deploying *the most innovative product ever created*: sensors precision and my own knowledge of 3d modeling and other designer abilities are limited. I want to keep the effort proportional to the learning, trying to avoid stressing over impossible goals

**What i would like my project to include**
- Fun: I want my project to be fun, both for me to learn and for people to use
- Magic: I want the project interaction to have the same effect as magic: surprising and not predictable
- Lights, because i like leds
- Biomaterials?

**Possible ideas - not separate, maybe could overlap?**
An experience of my idea - letting other people experience my idea of a connected, systemic concept of wellbeing, an ongoing discourse between us and the cities we live in.

An embodiment of my idea - an objects that actually works around the same logics I envision for my project. A wellbeing machine that works around
A wearable -

no raise awareness, acting on this
