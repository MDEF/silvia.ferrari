---
title: Mechanical Design & Machine Design
period: 10 - 17 April 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---
During the master's first term we had a class called [From bits to atoms](https://mdef.gitlab.io/silvia.ferrari/one/from-bits-to-atoms/), which served as an introduction for Fab Academy.

The assignment for the class was to divide in groups and build a machine.

The challenge was to create something using one input, one output and two digital fabrication processes. Me and my team (Gabor, Julia, Saira, Ryota) decided to work with music. We brainstormed for some time, deciding to create something that would:

- Make many different sounds
- Mix those different sounds
- Make music through movement
- React to light or weight

Our concept was to have drawings (black or white lines) as an input and music as an output. We researched similar projects online, and also asked to the week’s teachers how we could develop our idea. We documented our project in the same way we will have to do for the Fab Academy, and our project’s development repository is available [here](https://mdef.gitlab.io/scribble-sound/).
