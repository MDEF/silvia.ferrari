---
title: Embedded programming
period: 27 March - 4 April 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

For this week's assignment, I am going to program the board [I made for the **electronics design** assignment](https://mdef.gitlab.io/silvia.ferrari/fabacademy/pcbdesign/). The board has two LEDs on it, and my goal is to turn them on.


## Some theory

Understanding how to approach the week's assignment took me a while.
First of all, to understand how to program a board, I have to understand what I am actually dealing with. The board I programmed contains an ATtiny44 microcontroller, which works as the board's brain.

To read the ATtiny44 datasheet I was greatly helped by [this past FabAcademy student](http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/embedded-programming/) who has great documentation on electronics.
First of all the ATtiny44 is a **microcontroller**. Microcontrollers and microprocessors may get confused with each other, but they're different. Basically, a microcontroller is a small, simple computer. While a microprocessor contains a CPU but then needs to be connected with a peripheral RAM (Random Access Memory) and ROM (Read-Only memory), a microcontroller (MCU) has everything in it, and can perform small, specific tasks by itself.

On the ATtiny44 datasheet - which is available [here](http://fab.cba.mit.edu/classes/863.09/people/ryan/week5/ATtiny44%20Data%20Sheet.pdf) I found a diagram explaining the data memory's structure of the microcontroller and a map of the pins.

![]({{site.baseurl}}/datasheet.jpg)

To program the board we are going to use C language. C is a computer language that allows us to program instructions into a board. Arduino IDE uses a simplified version of C language, called C++ and also allows to download code libraries, which make it easier for us to use the code.

## Troubleshooting

First of all I have to check that the board works. I use a multimeter to check the path on the soldered board, discovering that:
- The resonator's soldering touches the path to the microcontroller
- The button's soldering touches the GND traces

![]({{site.baseurl}}/troubleshooting.jpg)

With the help of Santi i soldered the resonator again and polished the button's soldering. Santi also polishes a bit my soldering on the 6 pin plug and everything works fine.

## Connecting

I open Arduino IDE and the first thing I do is to download the Attiny library by going in *Tools > Manage Libraries and searching for ATtiny*

Then, I plug the board, which has to be connected to the computer through a six pin cable and also to the programmer. To focus on the job that I have to do today and minimize my troubles, I am using the Lab's AVRISP as a programmer.

I plug both AVRISP and my Helloboard to the computer, and the AVRISP led goes red.

![]({{site.baseurl}}/IMG_3521.jpg)

Than, in *Arduino IDE > Tools* I:

- Open the **Blink** example code
- Select ATtiny 44 as a board
- Select ATtiny 44 as a processor
- Select External 20 MHz as a clock, since I have an external 20 MHZ resonator on the Hello board
- Select AVRISP mkll ads a programmer

![]({{site.baseurl}}/tools.jpg)

And then I press **Burn Bootloader**. What am I really doing?
I am enabling the AVRISP to program my board through a serial port - the USB.

The first time I try I receive an error message, but I realize it's because I've chosen the wrong ATtiny as a board: instead of 44 I selected 24.

![]({{site.baseurl}}/error.jpg)

I try again and this time the bootloader burning goes fine.

## Programming

My board has one led that powers up when the board is connected - the LED is connected to VCC. To make the other led work I need to send to the board a code from Arduino IDE

I start with the **Blink** basic code provided by arduino, but to run it on my board I need to modify the standard code adapting the ATtiny pin numbers to the Arduino ones.

I use [this conversion table](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week8_embedded_programming/attiny_arduino.html) to change the numbers: my second LED is attached to ATTiny  pin 6 - that means pin 7 for Arduino.

The code works, the second LED starts blinking and that is **SUCH A SATISFYING MOMENT**.

![]({{site.baseurl}}/blink.gif)

After that, I try to include the button that I have on my board to the code . My goal is to have the led blinking, but pushing the button would make it stop.

I add the button to the blink code and I repeat the ATtiny-arduino conversion for the button: on ATtiny it's pin10 so it should be pin  3 on Arduino

![]({{site.baseurl}}/blinkbutton.jpg)

Here is a link to download the [blinking+button]({{site.baseurl}}/Blink_Button.ino) code
