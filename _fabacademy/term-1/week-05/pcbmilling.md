---
title: Electronics production
period: 14 - 19 February 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

This week assignment was to make an in-circuit programmer by milling a PCB board, solder components on it and program it.


## 1. Introduction

First of all we received an introduction on soldering and milling in the FabLab. The introduction focused on:

- **Materials** (copper, electronic components): there are different kinds of PCB boards, organized by code: F1 is copper + paper fiber, F4 is a double sided PCD board made with epoxy resin. For our exercise, we used F1. Milling these kinds of boards it's not the only way to create a board: vynil could be used as well. Hand made boards are long to make, but the process uses no chemicals (the industrial process does)
- **Processes** (milling, soldering): the essential component of milling is the endmill. There are various sizes for it, measured in inches: 1/64 is really think, while 1/32 is thicker. The endmills can be changed in a machine through the collet. To lock a collet, it has to be rotated clockwise.
- **Troubleshooting**: to have a nice board, we've been told to offstet always 4 times. If a milling doesn't work out, a bad Z offset, a worn endmill and too much dust interfering with the cut are among the most common reasons.

![]({{site.baseurl}}/soldering1.jpeg)

## 2. Milling

The first part of the assignment was to mill a PCD board. To learn how to do it correctly, I read about the process through other student's documentation, especially [this one](http://fab.academany.org/2018/labs/barcelona/students/oscar-gonzalezfernandez//2018/02/21/Week-04-Electronics-production.html) and I was guided through my first attempt by my classmate [Rutvij](https://mdef.gitlab.io/rutvij.pathak/), who did the Fab Academy last year. His tutorial was especially useful because he highlighted the most critical parts of the process and shared some useful tricks.

Once I selected the board to mill - **hello.ISP.44** - I dowloaded both the traces and outline from the [Fab Academy week's page](http://academy.cba.mit.edu/classes/electronics_production/index.html).

**RML file**

First I had to create the **.rmv** file, which is the one supported by our Roland milling machines, first to do the traces and then the output. To do so, I used the **FabModules** app two times, one for each file.

1. Import into FabModules the png file
**Tip**:check the size of the board after import, it happens that the scale is wrong

2. Select as an output **Roland**, which requires an .rml file

3. Select the **process**: to mill the traces select 1/64 (thin traces), to mill the outline select 1/32 (thick traces)

4. In the **right part of the screen**, select the output machine and check the other parameters. All of them fill automatically, but **Standard speed** should be 4 and **machine origin** should be 0,0,0.
**Tip**: check always that **zjog (mm)** has a >2 value. This is the measure of the up/down movement the mill will do while moving around on the board.  

![]({{site.baseurl}}/rml.jpeg)

5. Press **calculate**. The settings are standard, the most important to check is **offset** - the number of times the mill is going to drill the same surface. Standard is 4 but it can be changed. The images shows the number of offsets in black as well as the jogs, in red.

**Tip**: check now the origin of the file file, to be sure there is going to be enough space from the origin to the actual cutting

![]({{site.baseurl}}/traces.jpeg)

6. Press **save**

7. Prepare the endmill (the machine doesn't make a difference). Select the 1/64 for traces (first thing to do) and 1/32 for outlines. Check that it is not broken.

8. Prepare the board by sticking it to the mill with A LOT of tape.

**The machine**

The machine I used for my first attempt was the **Roland SRM-20**. On this machine, the procedure to start cutting (again, first the traces and then the outlines) is the following:

1. Adjust the endmill by raising the *z*, lowering it by unscrewing from the collet. Pay attention that the endmill is aligned

2. Position the endmill in the desired 0 position through the software

3. Press **x/y** and  **z** to set the parameters as the chosen ones

![]({{site.baseurl}}/coordinates.jpeg)

4. Press **cut**. A window will open through which it is possible to select the .rml file created with Fab Modules. Select it and click output, the process will begin

![]({{site.baseurl}}/milling.jpeg)

The first time I tried I saw something strange: the *z* was not deep enough, I noticed because there wasn’t enough powder on the board during the milling. I lowered the endmill manually as described above.

![]({{site.baseurl}}/pcb.jpeg)

After I milled my first PCB, I tried with Barbara and Rutvij to mill 3 at a time. We modified the traces and origin files in Illustrator and did the whole process. I did the mistake of leaving the mill unattended for some time once it started, and when I came back I discovered that the 1/64 endmill had broke, the board had moved and that of the three PCBs none was ok to use.

Since I wanted to make a second one, I tried using another machine, the **MDX-20**. This one is more difficult to use, as the interface is less friendly, but in the end I liked it more than the first one. I got the help of the Fab Lab tutors and I discovered that the process is similar to the first one, but:

- The Z has to be set manually, using the up/down buttons on the machine
- The X/Y have to be set through the software, moving a point in a space.
- The coordinates have to be set each time, one for the traces and one for the origin.

![]({{site.baseurl}}/interface.jpeg)

![]({{site.baseurl}}/buttons.jpg)

**Tip**: To be sure that both traces and outline have the same origin, drill a small hole in the board the first time. To do the outlines, just set the endmill over the small hole to have the same origin point.

![]({{site.baseurl}}/origin.jpeg)

**Other tip**: always clean the machine with a brush after use

![]({{site.baseurl}}/pcb2.jpeg)

## 3. Soldering

Soldering needs precision, but it's an entertaining exercise. For my board, the whole process was done in two steps

**Collecting the components**

The list of components for my board was available through the [Fab Academy tutorials](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/fabisp.html), which I found in the main website's documentation. The same tutorial is really useful as a walkthrough into soldering, because it explains the process step by step.

I made on my notebook a list of the components and then collected them. I placed a small amount of tape next to each item in the list and placed the components accordingly, to be sure not to lose them.

**Soldering**

I did a small soldering practice at the beginning of the week, but again Rutvij explained to me a few strategies to solder effectively.
To solder, the necessary instruments are: soldering gun, tweezers, suction pump and copper wire for excessive soldering. Components have to be solded:

- One at a time
- Either from center to side or to most to less difficult

I also found it really useful to work with three tabs opened on my laptop: the tutorial, the [board diagram](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) and [a photo of the completed board](https://www.as220.org/fabacademy/images/tutorials/fabISP/FabISP_IMG_0220.jpg), to check where and how to place the components correctly.

![]({{site.baseurl}}/solder.jpeg)

I think I will do better with time: I am especially clumsy at soldering the first part of a component: this is why my board is full of soldering balls.
