---
title: Invention, Intellectual Property, and Income
period: 28 - 3 May 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---
The last class of Fab Academy is about deciding the economical terms to make our projects available to a bigger public.

This entails reflecting on which type of licensing we would like to apply to our ideas, as well as reflecting on business models to get money back from our ideas.

The week's topic fits well for us MDEF students, as we are having at the same time a class called **Emergent Business Models**, which is run by the team at **Ideas for Change** and is challenging us to define the inner value of our projects, as well as a path towards making our idea economically sustainable and impactful from a social perspective in the short to mid period.

The model proposed by the Ideas for Change people is called Pentagrowth and aims at enabling **exponential growth** for our ideas leveraging on the ecosystem they belong to. I think it's important to highlight that by using the term *growth*, the model doesn't mean only economic growth, value in general, meaning also growth in the impact capacity of our projects.

![]({{site.baseurl}}/pentagrowth.jpg)


Leveraging on the ecosystem is a smart strategy: competitive advantage doesn't make sense in a world where knowledge is globally connected and ideas can be replicated easily all over the world. What makes it possible to create impact is the **ability to connect values and goals** trough:
- Connecting the network
- Collecting assets
- Empowering users
- Enabling other partners
- Sharing the knowledge
into the ecosystem we move in with our ideas through steps that represent the growth process.


I think this perspective makes sense, especially for **Doctors of the Future**, my project which is described more in detail [here](Doctors of the Future).
My idea doesn't bring anything new to the table in terms of technology, it leverages on shared knowledge and technology and combines elements that are already present in the system to create a new solution and a methodology to tackle an emergent issue.

## Creative commons licenses


Understanding the nature of my project and my ideas is fundamental to navigate into the Creative Commons licensing system, which are the only one I am taking into consideration for myself.

Even if as  MDEF students, we already have made an agreement with IAAC on our licensing, that states:

*“The copyright license regulation for the projects developed in the MDEF program inherent to the Fab Academy program will follow the regulation of the Fab Academy Program: Default license for student’s work will be under Creative Commons: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) Students interested in researching further can change their licensing if expressly mentioned.”*

I want to be able to understand what that means and decide how to mode forward.

The Creative Commons licensing is based on six licenses, which can be ordered from the most to less free, which are created combining four conditions:

- **Attribution (by)**: letting others copy, display and perform the work and make derivative works and remixes based on it only if they give the author or the credits in the specified ways

- **ShareAlike (sa)**: letting others distribute  derivative works only under a license identical to the license that governs the original work. This is important to avoid derivative work to be tied with more restrictive clauses

- **NonCommercial (nc)**: letting others copy, distribute, display, and perform the work and make derivative works and remixes for non-commercial purposes

- **NoDerivatives (nd)**: letting others copy and distribute only the original work, no derivatives or remixes

![]({{site.baseurl}}/creative-commons2.jpg)


Using [the flow provided in class](http://creativecommons.org.au/content/licensing-flowchart.pdf), I understand that the license I would like to use is the **Attribution Noncommercial-Share
Alike license**, which:

- Will let other people copy and distribute my work
- Will allow them to remix it, but only if the remixes or new versions will be available at the same conditions my work is available
- Will prevent them from making money from my ideas

![]({{site.baseurl}}/download.jpg)

## Next steps for my project

It's difficult for me to understand how to make money with my project. At its full, Doctors of the Future aims at creating a public office dedicated to monitoring collective health in urban environments and acting on the data gathered.
Reaching that point requires experimentation in terms of:
- Engagement from urban communities, healthcare practitioners and urban policymakers to shift knowledge on systemic health
- Development of data frameworks ensuring ownership to the data producers to shift our behaviors regarding data sharing
- Development of tools to gather information on the collectivity to shift in the infrastructures we use
Transitioning from the speculative idea of Doctors of the Future to its full potential requires a scalable model, to allow continuous improvements of the experimentation and a progressive engagement of the urban communities.

Once started, the intervention could definitely generate money for the stakeholders involved, but the first phase of implementation requires doing experiments and, because of this , external funding.

The first actions to perform to develop Doctors of the Future are:

- Creation of an open source model to distribute knowledge and allow everybody to experiment with the same methodologies used by Doctors of the Future.

- Development of a pilot model in three phases: definition, action and cycle improvement. The definition phase should be carried:
  - Involving practitioners from the healthcare sector, data scientists and makers to develop the tools and a framework to assess and expand the systemic illnesses catalogue
  - Defining a pilot community, assess their needs and co-create a framework for the intervention involving healthcare professionals and representatives from the community
  - Defining a data framework for the community to retain ownership of the data produced and anonymized access  for the health practitioners and the data scientists
  - Prototyping the tools with practitioners from healthcare, data scientists and experts from the makers’ community
