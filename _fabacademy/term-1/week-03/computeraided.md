---
title: Computer aided design
period: 31 January - 6 February 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

**Week assignment - evaluate 2D and 3D softwares, model a possible final project, document the process and post the original 2D and 3D files**

## My experience
This week's assignment is challenging for me, as I have no previous experience in 3D modeling, apart from the IAAC precourse on Rhino back in September. As for 2D, I know how to use Photoshop, Illustrator and a bunch of other softwares but I would consider myself to be a beginner.
Luckily I really enjoy learning, and I liked the brief induction we had on Fusion 360, which has a friendly interface and seems to me a lot easier to use than Rhino.

For this assignment, I'm planning to experiment with modeling lamps in 3d that could be produced by laser cutting and 3d printing

## Learning the tools
As a first introduction to 3D modeling, I dived into some Fusion 360 tutorials. I both watched those on AutoCad website, and searched a bit more online. [These](https://warwick.ac.uk/fac/sci/wmg/about/outreach/3d_design_printing/fusion_tutorials/)fast tutorials from the University of Warwick seemed a good start. So I designed a paperclip.

![]({{site.baseurl}}/paperclip.png)


## Laser cut lamp
I find Fusion pretty easy to use, so i start modeling the first of the two lamps following [this tutorial](https://www.youtube.com/watch?v=twcU6tWFhnc) which introduces laser cut modeling and Slicer for Fusion.

## Making solids
The first thing I learn is how to create a solid shape by using [Revolve](https://knowledge.autodesk.com/support/fusion-360/getting-started/caas/screencast/Main/Details/4270846d-e983-4615-b37b-fae4d5aa367c.html) and [Loft](http://help.autodesk.com/view/fusion360/ENU/?guid=GUID-9310FE7D-9AD4-40B4-A51A-373DA43B7A4A) commands.

To create a solid using **Loft**, I need to create three horizontal planar surfaces one on top of the other. The one in the middle is going to be a bit bigger than the bottom and top ones. To do so:
- Sketch a 80x80mm square
- **Offset the plane** 50 mm on top of the square, to create the middle surface and then offset again 50mm to sketch the top surface

![]({{site.baseurl}}/Offset.png)

To create a solid from the three surfaces, I choose the **Loft** command from the Create menu

![]({{site.baseurl}}/Loft.png)

To create the laser cutted lamp, I need the solid to be hollow inside. The command **Shell** allows to create an internal void, whose thickness I set at 20mm

![]({{site.baseurl}}/Shell.png)

To create a solid using **Revolve**, I need to sketch three lines connected by an arc. To make it easier, I set the view on the project on the right.

![]({{site.baseurl}}/Lines.png)

To create a solid from these lines, I use the **Revolve** command, select the profile and axis and create an egg-shaped solid

![]({{site.baseurl}}/Rotate.png)

Last, I shell the solid in the same way I used before

![]({{site.baseurl}}/Shell2.png)

## Slicer

Slicer allows to create amazing designs and it's really easy to use. I could designed sliced designs from both of my solids, deciding
- The construction technique (i chose radial slices)
- The amount of horizontal and vertical slices

![]({{site.baseurl}}/Loftsliced.png)

![]({{site.baseurl}}/Rotatesliced.png)

Anyway, I found a bug in the process that I hope to be able to solve soon: when I move from Fusion to Slicer, my designs rotate on the side and it's impossible to move them back in the original position. This affects the disposition of the slices and for this reason I couldn't finish my lamps.

Here are the files for the two solids, before the export in Slicer

[**Loft lamp**]({{site.baseurl}}/Loft.f3d)

[**Revolve lamp**]({{site.baseurl}}/Rotate.f3d)


## 3D printed lampshade
The second lamp I would like to model is a possible 3D printed lampshade with a nice pattern. I use [this tutorial](https://www.youtube.com/watch?v=3PnKBSOulwo) as a guide trough the process.

First of all, to create the lampshade I decide to revolve a planar surface I designed

![]({{site.baseurl}}/Planar.png)

![]({{site.baseurl}}/Revolve.png)

From the tutorial, I learn that to create a sturdy lampshade, it's necessary to leave some of the design untouched by the pattern, on top and on the bottom. To do so, it's necessary to cut the solid in three parts, leaving 8mm on top and on the bottom by using again **offset** and then **split body**. With this procedure, I no longer have only one body but three.

![]({{site.baseurl}}/split.png)

The next step is to model the solid to contain a lamp. To do so, I:
- Work on the upper body creating a smaller circle on top of the lamp - leaving 10 mm of thickness - and then **Extruding** it to the central body. This way, if I'll change the position of the construction plane i set before, the extrusion will adapt accordingly.

![]({{site.baseurl}}/extrude.png)

- Work on the central body using **Shell**, leaving a 1mm thickness

![]({{site.baseurl}}/shell3.png)

To create the 3d pattern on the central body of the lamp, I first hide the top and bottom parts.

Then I **Project** the geometry of the solid, so that even if I hide the central body, the shape is still visible

![]({{site.baseurl}}/project.png)

On the projection, I create two line which will become the pattern's starting points and make them parallel by constraint. I also set 6mm distance between them.

![]({{site.baseurl}}/constraint.png)

To create the pattern, the next step is to transform this profile into the solid. To do so, I use the command **extrude** and the option *intersect*, basically cutting the solid.

![]({{site.baseurl}}/intersect.png)

Now I can create a pattern from the parallel lines by selecting the command **Pattern** and specify *+circular pattern* using both the lines as the body to replicate

![]({{site.baseurl}}/pattern.png)

I make the top and bottom bodies visible and here's the lampshade

![]({{site.baseurl}}/lampshade.png)

[**Lampshade**]({{site.baseurl}}/lampshade v1.f3d)
