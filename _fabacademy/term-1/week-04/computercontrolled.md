---
title: Computer controlled cutting
period: 7 - 13 February 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---


Again, this week's assignment is challenging for me as I have no previous experience in any of the topics covered. Anyway, I am improving my Fusion 360 skills and I really enjoy the creative side of Fab Academy, so I'm eager to learn new things.


## Laser cutting

For the laser cut assignment, I would like to work with spheres and acrylic but since I'm a total beginner I guess it's better if I start with paper or cardboard.

First of all, I create with Fusion 360 a triangle using parametric design. I go through it with my classmate [Gabor](https://mdef.gitlab.io/gabor.mandoki/).

![]({{site.baseurl}}/triangle.png)

Designing the triangle I learn that I can create parameters for my designs: assigning them to specific parts of my design and modifying them, the whole design will adjust accordingly.

![]({{site.baseurl}}/parametric.png)

My first idea is to work with Fusion creating more parametric shapes, and eventually create a sphere or a geodome. Looking online I find some useful material to start with, especially browsing previous Fab Academy student's websites

- [Geodesic lantern](http://archive.fabacademy.org/2016/fablabtrivandrum/students/424/w3-cutting.html)
- [Press-fit geodesic dome](http://infosyncratic.nl/weblog/2008/09/16/press-fit-geodesic-dome/)
- [Geodesic dome](http://fab.academany.org/2018/labs/fablabkamakura/students/jun-kawahara/exercise04.html)
- [Geodesic lamp](http://fab.cba.mit.edu/classes/863.18/Harvard/people/david/week2_laser.html)


Since my time is limited this week, I ended up deciding to continue what I started during [last week assignment](https://mdef.gitlab.io/silvia.ferrari/fabacademy/computeraided/), printing the egg-shaped lampshade that I modeled with Fusion 360 and Slicer.

Last week I had an issue with Slicer that I couldn't solve: once in the software, my lamp was positioned on one side. I couldn't solve this bug and in the end, with the help of the tutors, I decided to simply rotate the solid in Fusion so that it would appear in the right position once in Slicer

![]({{site.baseurl}}/lamp.png)

To prepare a file for the laser cutter on Slicer:

- I adjust the *manufacuring settings* according to the laser cutter - I am using **Trotec 400** - and the material I am going to use. With millimeters as unit the material thickness is set at 4, while height and lenght are the same dimension as the cardboard sheet i'm going to use. I set the ***offset*** at -0.01, to adjust to the material that the laser is going to destroy. These settings are parametric: once I set them, the whole design is automatically going to change accordingly.

![]({{site.baseurl}}/settings.png)

- I set the *object's size, construction technique and slice distribution*. Slicer allows to visualize immediately the changes i'm making and it calculate the amount of cardboard sheets that i'm going to need to cut all the parts.

![]({{site.baseurl}}/settings2.png)

- Once the settings are done, I *export the plan* in ``.dxf`` format

![]({{site.baseurl}}/export.png)

The next step is to open the file in Rhino to adjust it and prepare it for the laser cutter. I realize that the shapes that I have made cannot be selected as single curves, so I join all of them into just one shape using the command ``join``. I also add all the shapes into one level -that I mark in red - called *cut*. For some reason the shapes appear blue rather than red, even if the level is the correct one. I think it is a bug and I continue preparing for the cut.

I adjust the power, speed, and hertz parameters. I use the standard values suggested, but learn from other classmates experimenting with 4mm cardboard that the optimal power is 35 instead of 30.

I put my cardboard into the lasercut, I tape it to the borders and press start. I immediately notice that the cut takes much more time than it's supposed to, around 30 minutes. I also notice that the machine does not perform a straight cut, but many small ones. I think it's weird and I wonder if i joined all the curves into the file correctly. After a while I notice that the cardboard is not placed correctly, and the laser is cutting outside it. I stop the machine and decide to review the whole file.

![]({{site.baseurl}}/error1.png)

When I open it on Rhino, I immediately notice that something is wrong: selecting one of the lampshade's parts is impossible because the drawings are still not joint.

![]({{site.baseurl}}/error.png)

My classmate [Nico](https://mdef.gitlab.io/nicolas.viollier/) comes to rescue teaching me a workaround around this problem:

- Select the whole object I need to join
- Type the command ``Make2D``
- Move the resulting blueprint to the side
- Select it and use command ``join`` on it

Once this error is solved, all I need to do is to find another 4mm piece of cardboard, cut it the right size, adjust again the cutting parameters and start.

This time everything works fine! I made my egg-shaped lampshade real.

![]({{site.baseurl}}/eggshape.jpg)


## Vinyl cutter

I decide to create a sticker based on my Material Driven Design class, where I am working on creating a material out of shrimps.

To create the sticker I look for a nice shrimp image to start with. The first one that I like it is probably too difficult for the machine to cut, as the many leg of the shrimp are too tiny.

![]({{site.baseurl}}/shrimp1.png)

I  set on a less realistic shrimp image by looking on [Flaticon](https://www.flaticon.com/search?word=shrimp)

![]({{site.baseurl}}/icon.png)

and modify it with Illustrator to meet my requirements: I add some details and insert the wording **SHRIMPS!**.

To prepare it for the vinyl cutter:
- I isolate the outlines of the sticker and transform them into a path
- I save the file in ``.dxf``

![]({{site.baseurl}}/illustrator.jpg)

On the FabLab computer controlling the vinyl cutter - i used **Silhouette cameo**:

- I open the .dxf file
- I input the measurements of the vinyl piece i'm going to cut
- I resize my drawing to fit four shrimps into the vinyl piece
- I press start

The result is great!

![]({{site.baseurl}}/final.jpg)


## Downloadable files

[**Parametric triangle**]({{site.baseurl}}/parametrictriangle.f3d)
[**Egg-shaped lampshade**]({{site.baseurl}}/SFlasercutMDEF.dxf)
[**Shrimp sticker**]({{site.baseurl}}/shrimp.dxf)
