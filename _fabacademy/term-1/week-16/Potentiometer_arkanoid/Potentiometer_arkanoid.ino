int potPin = 3;  //Pin3 is the pin where we read the analog value from the potentiometer


void setup() {
  Serial.begin(9600); //Here we start reading

}

void loop() {
 int val = map (analogRead(potPin),0,1023,0,590); //Here we read the value from the potentiometer in a range
Serial.println(val);
delay(50); //this is the very short delay between one reading and the next one
}
