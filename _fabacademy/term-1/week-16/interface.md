---
title: Interface & Application
period: 9 - 16 May 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

For this week's assignment I am keeping it simple. I've never used Processing, never ever tried to interface anything so I will keep it basic to learn the logic behind it.

Eventually, if I have time, I would like to learn how to visualize the data that I'm gathering for my IAAC experiment, so either data from smart citizen kits or from the EMF sensor that I am building.

But for now, let's keep it simple.

I will try to make a game with my Arduino, a potentiometer and Processing. There are many options online, the one I like the most [teaches how to recreate Arkanoid](https://www.hackster.io/matejadjukic03/potentiometer-game-05ee93), an 80's game also known as Bricks.

The process is done in three steps:

## Building Arduino

The hardware is minimal. We simply have to connect the potentiometer to Arduino following this scheme

![]({{site.baseurl}}/scheme.png)

- On the board, the central pin of the potentiometer has to be connected with one of the **analog pins** on Arduino.
I connect it to **A3**

- The left and right pins have to be connected to **power(5V)and ground**. It doesn't matter which one

![]({{site.baseurl}}/IMG_3493.jpg)

## Coding on Arduino IDE
The Arduino code is really basic, simply a reading of the potentiometer's value. The values will let us move on the game interface

[**Downloadable Arduino code for the potentiometer**]({{site.baseurl}}/Potentiometer_arkanoid.zip)

## Coding in Processing
Processing is the software allowing us to create the interface of the game by coding it.
Since I don't know how to code in Processing, it's better for me to look at a finished code and understand what is being done by reading it. The tutorial provides all the Processing code, which is very detailed:

- There are two different colors for the blocks to break
- The platform that moves following the potentiometer values gets smaller each time I fail at catching the ball


The code needs to be changed in one part: the **port** from which we're reading the serial numbers has to be changed from COM12 to the name of the actual port used. I know I can find it in Arduino IDE under *Tools>port> /dev/cu.usbmodem143101*.

[**Downloadable Processing code to create the game**]({{site.baseurl}}/arkanoid.zip)


Also, an image has to be added as background. The tutorial provides the Arkanoid logo, but anything will do. To add it to the game it's important to save it on the computer, add it in the code in *img = loadImage("imagename")* and then also add it to the process by going on *Sketch>Add file..*

After it, we can click play and the game will begin!

![]({{site.baseurl}}/akranoid.gif)
