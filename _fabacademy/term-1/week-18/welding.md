---
title: Wildcard week! Welding
period: 20 - 27 May 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

![]({{site.baseurl}}/welding.gif)


## Welding machine

Melting metal through a welding machine works through electricity. The welding machine is polarized: the filament to solder, which comes out from a welding gun, has a positive charge. A corresponding negative charge is provided by a ground cable, which has to be attached to the metal surface we are working on.

The welding machine also uses gas, which comes out from the gun together with the filament. Gas allows to create a mini environment around the welding.

The welding machine we use works with steel and aluminium

## Safety first

There are some instructions to follow to prepare for welding:

- Put a jacket on to protect from sparkles & leather gloves to protect from the sharpness of metal. Rubber gloves cannot be used because they melt with heat. Leather gloves however only partially protect us from heated metal, so we should always pay attention when handling it.

- Always wear a helmet. Welding helmets are very dark, to protect our eyes from the intense light coming from welding. There are two kinds of helmet: the bad one - which is always very dark - and the good one - which becomes dark once it is exposed to intense light. Bad helmets do not allow you to see anything apart from the welding light, while with good ones you don't completely lose sight.

![]({{site.baseurl}}/IMG_3527.jpeg)

- Pay attention at hair, they should be protected

- Pay attention moving around the welding machine. The cable connecting the gun to the machine contains the filament, so if the cable is bent the filament will break

- Before starting welding always ask people around if they are ready - meaning if they have a helmet on to protect them from the intense light. Also wait for the answer.

- Since the gun is polarized, letting it touch metal will produce sparkles. Always put it away in the right way (not leaving it on the table)

![]({{site.baseurl}}/IMG_3526.jpeg)

## How to do welding

- Take out a small amount of filament by pressing the gun. We should always have a few amount of filament, otherwise instead of welding we will produce small balls of metal.

- Look if the filament coming out has a ball on top. That shouldn't be, so if there is a ball we should cut the end.

- Regulate power (the voltage we are using) & thickness of the filament on the machine. The thinner is the filament, the less power we should use. We used 2/3 mm thickness for the filament

- Prepare putting the helmet on and the gun in hand, attached to the metal to be melted

- Start welding staying close to the materials, otherwise it will be impossible to see. Move slowly in small circles, so to avoid creating balls

- To be sure of the process, listen to the sound. There is a constant sound that the machine should make.

- Pay attention at the color of the metal: with very thin metal, red welding could means having a hole in the welding

![]({{site.baseurl}}/IMG_3528.jpeg)


## How to finish welding

Once soldered the metal together, we can sand it with the sanding tool for metal. The process is pretty simple and straightforward, but i've been reminded that I have to keep my hair up because the machine's rotation could catch them.

Also, it's important to orientate the job in a way that lets the sparkles come towards you, not all around. This is a safety measure.


![]({{site.baseurl}}/IMG_3532.jpeg)
