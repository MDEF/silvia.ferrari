---
title: Molding & Casting
period: 21 - 28 March 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---
As we learned during class, the molding & casting process is a three steps process:
- One: design and creation of the object through 3d printing or wax milling. If at least one side of the object is flat, it is possible to design it on a flat surface in one part. Otherwise, to be able to mold it in 3d correctly, a two-parts design is required.
- Two: molding the negative of that object.
- Three: casting the object with the material of choice

For this assignment, I work together with [Jules](). Since she's expecting the Fab Baby, she cannot be in contact with chemicals, so we will share responsibilities and I'll handle the molding and casting for both of us. To keep it simple, we decide to design rings. Since at least one side of the rings is flat, we are able to design it in one part. We take inspiration from a [previous FabAcademy student from Korea](http://archive.fabacademy.org/fabacademy2017/fablabseoul/students/385/week13.html), which designed rings and casted them with epoxy resin. I've seen similar rings on Etsy and for a long time I have wanted to buy one, but it's a good occasion to try and make them myself.

![]({{site.baseurl}}/etsy.png)


First we measure of our fingers, and, since we estimate to have space for 8 rings in our mold, we also ask our classmates if they want some rings for them.

- Julia: 53mm, 57mm
- Silvia: 63mm, 58mm
- Fifa: 57mm
- Jess: 57mm

Diameters:
17 mm, 18,2 mm, 20 mm, 18,5 mm

We decide to prepare our mold by wax milling. We find a unused wax brick that suit us perfectly. Its dimensions are **14,5 cm X 8,5 cm X 2,5 cm**, and our 8 rings should fit into it.

However the brick's surface it's not perfectly flat. This could hamper the process, since an uneven surface may not stick correctly to the milling machine. So we use the big CNC machine and Rhino Cam to flatten it out.

## Molding with wax

The second step of the process is designing the rings to create the wax mold. We start with Rhino. but soon switch to Fusion because it has such a friendly interface and I don't want this part of the process to take too much time.

I start by designing a rectangle the same size of the wax brick and then extrude it to the brick's height.
The process then requires to:
- Design a smaller rectangle inside and then lower it
- Design circles for the rings, one internal - the size of our fingers, one external - the desired thickiness of the rings (5mm)
- Extrude them for the desired length of the ring  
- Lower the central part of the ring to ensure that the epoxy resin won't spill out while casting

I realize while doing it that it's better for me to take note of the measurements in a sketch before starting to design, otherwise I get easily lost and end up messing everything up.


![]({{site.baseurl}}/IMG_3490.jpg)


Once the design is ready we start preparing the milling machine, cleaning it from wood dust and replacing the wooden base with a plastic one which is better to stick the wax brick.

The milling process for wax is divided in two steps. First the **rough cut** with a flat endmill and then the **finishing cut** with a rounded endmill. The size of both endmills is 1/8

To prepare the files tor the **Roland SRM-20** machine, we use FabModules. We input the values using both [Jess' documentation]() and advices from Edu, because for some reason FabModules is really slow at calculating our file's paths.

For the rough cut:
*Input*
- Select the .stl file
- Set **unit** at 25.4
- Set **dpi** at 500
- Look that x, y and z are set to 0
- Calculate Height Map

*Output*
- Select **Roland** and **Wax rough cut**
- Set **speed** at 20
- Look that x, y and z are set to 0
- Set **zjog** at 6

*Process*
- Set direction as **conventional**
- Set tool diameter at 3 mm
- Calculate
- Wait ages for FabModules to actually perform the job
- Save the file

We do everything one time before realizing our file is oriented vertically, while in the milling machine the brick is oriented horizontally.
We rotate the file in Rhino through the *rotate* command, and save it with the *binary* option, otherwise the file is not recognized by FabModules.
We then do the process all over again, but the second time it takes ages. Once finished, we set the x,y and z on the roland software but as the process starts we immediately notice that the milling hasn't started in the middle of the brick as it should have, so we cancel the job.
Edu helps us in understanding what happended, and in the process he shows us that **to check if the file is ok, it is possible to calculate the milling path faster with FabModules by putting a lower dpi resolution**.

In the end, using FabModules on his computer all seems ok, so we start the process once again. The only difference is that Edu set the **speed at 10 instead of 20**, which slows down the overall process, but this time everything works out fine. Once finished the rough cut, we organize the finishing cut.

For the finishing cut in FabModules:

*Input*
- Select the .stl file
- Set **unit** at 25.4
- Set **dpi** at 500
- Look that x, y and z are set to 0
- Calculate Height Map

*Output*
- Select **Roland** and **Wax finish cut**
- Set **speed** at 20
- Look that x, y and z are set to 0
- Set **zjog** at 6

*Process*
- Set direction as **conventional**
- Set tool diameter at 4 mm
- Calculate
- Wait ages for FabModules to actually perform the job
- Save the file

On the Roland mill:

- Change the endmill (from flat to round)
- Upload the finishing file created with FabModules

Overall, the process goes really well, but we make a small mistake and one of the two sides of the mold is really thin, just 2 millimeters.

I soon break a small part of it by mistake, but the damage is small and I manage to repair it with some tape.


## Molding with silicone


Since we're late with the assignment, we're don't have that much choice in terms of molding materials.
After having looked at the leftovers from our classmates, we realize that we're left with
- The silicone that didn't work in [Ollie's first attempt to mold](https://mdef.gitlab.io/oliver.juggins/fabacademy/molding-casting/)
- Smooth-on Mold Max 15 T, a silicone rubber compound in two parts

We decide to try the second. On the [product's webpage](https://www.smooth-on.com/products/mold-max-15t/) we find all the instruction to mix the silicone correctly and prepare our mold

![]({{site.baseurl}}/IMG_3471.jpeg)

To create it, our process is as following:

- Measure the volume of the silicone needed with water. By pouring water into our wax mold, we understand that we need the silicone volume of around a plastic glass.
- Clean the wax mold with the air pump
- Spray the release agent on the wax mold - it needs to rest for 10 minutes before pouring the silicone
- In the meantime, we mix the component A and B of the Smooth-on Mold Max 15 T. The weighted proportion should be 10A:1B. So we put roughly the volume of component A we know we need into a glass and weight it. For 144g of it, we add 14g of component B and stir for 3 minutes.

![]({{site.baseurl}}/IMG_3482.jpeg)

After that, we start pouring the mix into the wax mold. Everything goes smoothly but since silicone is very thick, I'm worried about leaving air bubbles into the internal holes of the ring molds.
To solve this issue, our classmate Rutvij teaches me a trick to ensure the silicone goes all the way down into the ring holes

![]({{site.baseurl}}/move.gif)


Since the material is so thick, it's really easy to create air bubbles and be left with a very uneven mold in the end. To solve this issue, we put everything under vacuum for 10 min using the FabLab vacuum pump

![]({{site.baseurl}}/IMG_3481.jpeg)

After it, the silicone is ready to rest for 24 hours

![]({{site.baseurl}}/IMG_3477.jpeg)

## Casting

After 24 hours the silicone mold is ready and taking it out of the wax is very satisfying.

![]({{site.baseurl}}/IMG_3492.jpg)

We decide to cast rings using:

- Epoxy resin
- Small components like dry flowers, gold paper, copper filaments

The epoxy resin we find in the FabLab has very easy instructions:

- Resin should be mixed with a hardener from the same brand. Hardeners could be slow, medium or fast, and according to which one is used the proportion of resin:hardener changes

- We use epoxy resin with a medium hardener. That means that, measuring by volume, we should have 3 parts of resin for 1 part of hardener

![]({{site.baseurl}}/IMG_3482.jpeg)

Once mixed, the resin heats up quite fast. We Google and find it is a normal behaviour

![]({{site.baseurl}}/resin.png)

Here, we make a small mistake. To be more efficient, we decide to mix the whole amount of resin we will need to cast our 8 rings, which amounts roughly to a full glass.
However, the process of casting is slower than we thought: first we cast a small amount of resin, then we put the components in the mold and then again we put some resin to fill it.

The resin is so difficult to handle without a proper extruder (like a syringe) and the components are so small that we manage to complete only three rings before the resin is so heated up that it's impossible to handle.

![]({{site.baseurl}}/IMG-2749.JPG)

We leave the three rings to harden and, since our classmate [Barbara]() is using natural resin to cast a necklace, we borrow some to make experiment with natural resin rings.

![]({{site.baseurl}}/IMG-2764.JPG)

Natural resin is so much easier to handle, it hardens very fast but the rings are very fragile and impossible to use.

![]({{site.baseurl}}/IMG-2772.JPG)

After 24 hours, the epoxy resin is hard and we can check how the three rings are.
They are perfect and only need some sanding before being ready to use.

![]({{site.baseurl}}/IMG_3498.jpg)


## How to improve the process

Now that we have the mold we can continue casting rings, but as we learned by doing, we should:

- Mix small amounts of epoxy resin and use them immediately. The process will be slower but the heating up will be minimal

- Use a syringe/an extruder to fill the mold, since the space is so narrow and the epoxy resin is very difficult to handle (it's sticky and very liquid)
