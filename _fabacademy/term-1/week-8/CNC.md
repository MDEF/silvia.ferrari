---
title: CNC milling
period: 6 - 12 March 2019
date: 2019-01-29 12:00:00
term: 2
published: True
---

CNC means computer numerical control and indicates a process where a computer translates a three dimensional design produced with a CAD software into numbers. These numbers stands as coordinates of a three dimensional graph. This way, they control the movements of a cutter, through which we can bring to life the three dimensional design.

In Computer Aided Machining, CNC milling is a kind of **subtractive production**, as laser cutting and vynil cutting are.

Our assignment for this week is to create a group and make something big on a CNC machine.

## Designing the T-Rex
I coupled with [Fifa](https://mdef.gitlab.io/fifa.jonsdottir/) and we decided to try to make a big dinosaur puzzle, similar to the ones kids have. We decided to use the whole board we were provided with (**1220X2440X15 mm**) to make it as big as possible.

The first step of our assignment was to find patterns to adapt to our needs. We could have designed the dinosaur's pattern from the beginning, as [this guy](https://www.instructables.com/id/T-Rex-Dinosaur-Puzzle-with-different-sizes-and-pos/)did, tracing a CAD file from an image, but due to the time constraint we decided to look for a completed template. On Thingiverse and Instructables there are plenty of dinosaur projects, but no one provides the templates used in the process. Finally we found the information we needed in these two projects:
- [Build a 6'-0" Tall Wooden T-Rex Model](https://www.instructables.com/id/Build-a-6-0-tall-Wooden-T-Rex-Model/)
- [Cardboard Dinosaur Puzzle](https://www.instructables.com/id/Cardboard-Dinosaur-Puzzle/)

Ultimately we decided to go with the files provided in the first one, as the tutorial seemed more complete and we preferred to create a T-Rex rather than a Pteranodon.

[T-Rex Guide]({{site.baseurl}}/trexguide.pdf)

The first step of our process was nesting all the dinosaur's pieces in one Illustrator file, which we would use to create:
- Our first prototype with the laser cutter
- The final Rhino file

![]({{site.baseurl}}/nesting.png)

There was a specific reason why we decided to test our idea through a prototype: one of the pieces in the template found online was odd, and we didn't understand how we had to cut it.

![]({{site.baseurl}}/piece.png)

We decided to eliminate the cloudy shape from that particular piece and see if it could work in our prototype

## Using Fusion and laser cutting a prototype
As neither of us knew how to use Rhino properly, we started our project using Fusion 360, as the interface is easier for a beginner.

Once opened the file in Fusion, our task was:

- To input parametric values for the dinosaur's joints size - we were unsure about how big they should be to ensure a good fit and we wanted the resizing process to be as fast as possible
- To make all the joint's lines perpendicular
- To scale the design to the board's measure (taking into account also the kerf). We decided to perform this task as the last one.

We immediately noticed how slow Fusion can be at processing information coming from a not native file, but we decided to continue anyway.
We started adding parametric values to the joints by clicking *modify > change parameters* and assign a 15mm value to the **w** parameter we would use for the joints' width.

![]({{site.baseurl}}/parameters.png)

We then selected the two vertical lines of each joint, right click ad select **parallels** to make sure they all were perpendicular.

![]({{site.baseurl}}/parallels.png)

Simultaneously to our Fusion exploration, we went on with the laser cutting process by:

1. Opening the .svg file on Rhino and preparing it for the laser cut. I had to review my [laser cut submission](https://mdef.gitlab.io/silvia.ferrari/fabacademy/computercontrolled/) because the curves appeared to be open in Rhino, a problem that can occur with native Illustrator files. As I learned in the past weeks, the best option is to:

- Select the whole object I need to join
- Type the command ``Make2D``
- Move the resulting blueprint to the side
- Select it and use command ``join`` on it


2. Prepare the laser cut by adjusting the settings to the material we were using:
- We adjusted the focus of the laser
- We set the laser parameters as indicated on the Fab Lab examples provided

![]({{site.baseurl}}/fablab.jpeg)

The first cut didn't work properly: the parameters we set - especially **speed** and **power** - were too weak to cut the wood so we tried again modifying them.  The second time we increased the power from 60 to 80 and the laser cut was perfect.

![]({{site.baseurl}}/trex.png)

We were able to check our prototype, and the process taught us two main things:

- We were right in eliminating the cloudy part from the piece we were unsure about
- When nesting, we didn't take into account the space we would need between the pieces for the CNC machine to be able to cut them properly. Even on the laser cut, the pieces were too close to each other. We needed to scale a bit the pieces on Fusion while keeping the joints' size to 15mm.

Immediately after starting working on this issue in Fusion, I realized there was no easy way out of it. The software was incredibly slow and the scale command took ages to perform its task. The problem was that the curves forming each piece were not joined together, and I couldn't find an easy way to solve this issue. The slowness of processing made it impossible to experiment with Fusion's functionalities. We decided to move everything to Rhino, even if doing so would have meant losing the parametric values of the joints.

Rescaling the pieces in Rhino required us:
- To join all the curves together using ``Make2D`` and then ``join``
- Rescale all the pieces to 0.8 of their original size
- Design a 1220X2440 rectangle with a 20 mm kerf
- Nest the pieces again into the rectangle being careful, since we wanted to leave a 20 mm space between each of them
- Modify the joint size one by one: by rescaling, we made the pieces smaller than 15 mm and since we lost the parametric measurement by moving to Rhino, this was the only possible way to resize them. To do so, we trimmed every joint, made a longer end for each one and joined each piece together again

Once completed this task, we felt we were ready to move to the next step, which would have been to prepare the file for the CNC milling with **Rhino CAM**.

![]({{site.baseurl}}/maybe.png)

A Fab Lab tutor made us aware of a mistake we made in our design:  during the whole process we forgot to add some dog-bones to the corners of each joint. This way, the CNC milling wouldn't be able to create fitting joints. So we added them, one by one, adding a 6mm circle to each joint corner and then trimming and joining each piece.

![]({{site.baseurl}}/circles.png)

## RhinoCam
Once the file was ok we started setting up Rhino Cam to make it ready for the CNC milling.

The job in Rhino Cam is to define all the possible parameters and movements of the machine needed. This is why the setting up process is done in three steps, one for each moment of the milling. We were greatly helped in the process by our classmate [Jess](https://mdef.gitlab.io/jessica.guy/):

**Engraving**: the first job to be done on the machine. In our case we needed it to define where to put the screws on our board to place it safely on the CNC milling.

- **Machine operation > two axes > engraving**
- **Select drive containment regions**: we define where the screws should be
- **Clearance plane**: controls the *Z* when moving from one job to the other, so it has to be higher than the material/every design
- **Cut parameters**:how the cut is exactly going to be. In **Cut direction** we choose *Climb(DownCut)*, and in *Total cut depth** we put 15mm (the board's size) plus 0.3 more, to be sure that the cut was clean.
- **Entry/exit**: here we put *none* for now
- **Sorting**: controls the movement of the machine between one step and the other. We chose *minimum distance sort*, which is the fastest way to get the job done
- **Tool**: here is where you choose the mill. Our 6mm mill was already selected so we didn't do anything
- **Feeds&Speed**: as in the picture below

![]({{site.baseurl}}/engraving.jpg)


*press generate*

**Pocketing**: the next job after engraving is to cut the *pockets*, meanings any hole planned in the design. We had just a few in the two head-pieces of the T-Rex.

- **Machine operation > two axes > pocketing**
- **Select drive containment regions**: we define which parts have to be pocketed, but to set all the parameters just selecting one is enough. The others can be added at the end of the process.
- Some of the parameters don't need to be changed: **Clearance plane, Tool, Entry/Exit, Feeds&Speed, Sorting**
- **Cut parameters**: as in the picture below

![]({{site.baseurl}}/pocketing.jpeg)

*press generate and then select all the pockets in the design*

**Profiling**: the last job is to cut the profiles of the pieces in the design.

- **Machine operation > two axes > profiling**
- **Select drive containment regions**: as with the pocketing, we need to select just one of the pieces we need to profile and we can add the rest later.
- Some of the parameters don't need to be changed: **Clearance plane, Tool, Entry/Exit, Feeds&Speed, Sorting**
- **Cut parameters**: everything is as before but we select *outside* instead of *inside*.
- **Advanced cut parameters**: as in the picture below. The aim here is to define how many bridges we will need for the pieces.

![]({{site.baseurl}}/profilin.jpeg)


*press generate and then select all the profiles in the design*

We chose all the pieces in Rhino CAM at once and this made the software really slow. Moreover, as we discover later, the file we were using had some very small and tiny lines here and there which made the calculation impossible.

Fab Lab people helped us cleaning the file by:
- Closing all the curves for each piece in the design
- Moving all the closed pieces into an invisible layer
- Selecting the surface and deliting the remaining, invisible pieces of design that prevented us from generate a Rhino CAM project

## CNC milling
We then moved to the machine and learned how to make it ready for each of the jobs we had to perform (engraving first and then  pocketing-profiling togheter)
The process requires to:
- Set X and Y. This can be done using the software - where we can choose the speed-  or the remote control, which is slower by design.
- Set the Z for engraving by running the **custom, set Z axis** script using a button
- Run the engraving. In the setip tab it is possible to follow the job and check errors

![]({{site.baseurl}}/cnc.jpeg)

- Screw the board
- Set the Z again for the pocketing+profiling through the same process
- Run the process
- Power the vacuum while cutting, so that the dust at the end it's easy to clean

![]({{site.baseurl}}/cut.jpeg)

## End of the CNC process
The last part of the job was to sand all the cutted pieces and put them togheter. Plywood turned out to be not the best choice of wood for our T-Rex, because it is quite heavy and very difficult to keep neat when cutting. The material is so weak that some smallest parts, like the T-Rex teeth, were impossible to sand without breaking.

We also realized we didn't calculate any offset for our joints, but we managed to put them together anyway. Our T-Rex is ready.

![]({{site.baseurl}}/ready.jpg)

## Downloadable files

[Ready to cut T-Rex Rhino file]({{site.baseurl}}/TREXcnc.3dm)
