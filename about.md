---
layout: page
title: About
permalink: /about/
---

Hi i'm Silvia!

For the past five years I worked in corporate innovation consulting, designing collaborative strategies to lead the implementation of new digital technologies.

Previously, working in politics and public administrations, I've had the chance to travel a lot: I've lived in Rome, New York, Paris and Hanoi.

I've always been curious about everything, and I like to be involved in as many thing as possible as well. This is why being an activist has always been part of my life. I collaborated with political parties, think tanks and volunteering association mixing my experience with new technologies with my passion for public engagement and social innovation. I had experiences both on the local level (designing bottom-up policy-making platforms and co-design events) and on the field (working in the Greek islands during the refugee crisis in 2015-16).

I'm interested in the the democratizing power of new technologies and their role in shaping the cities of the future, but these are just two of the things that i'm curious about. Among others: circular economy, digital fabrication, DIY biology
