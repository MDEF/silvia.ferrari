---
layout: page
title: Readings
permalink: /readings/
---

## Doctors of the future

## Exposome

* [Complementing the Genome with an ‘‘Exposome’’: The Outstanding Challenge of Environmental Exposure Measurement in Molecular Epidemiology]({{site.baseurl}}/documents/Complementing the Genome with an Exposome The Outstanding Challenge of Environmental Exposure Measurement in Molecular Epidemiology.pdf)
The first proposal for the study of the exposome

* [Use of the “Exposome” in the Practice of Epidemiology: A Primer on -Omic Technologies](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5025320/#R10)
  A 2016 summary on the medical research carried on the exposome, as well as on the tools used to explore the field, including reality mining

* [What chemicals or critters do you encounter daily? Stanford researchers are taking a look](https://medium.com/scope-stanford-medicine/what-chemicals-or-critters-do-you-encounter-daily-stanford-researchers-are-taking-a-look-99903b3353ee)
  Recent studies carried out at Stanford on the exposome through  personal tracking

* [Using Personal Sensors to Assess the Exposome and Acute Health Effects](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4143834/)
  Barcelona based experiment carried by ISG involving personal sensors to evaluate the impact of pollution and UV rays on individuals. The project used also the participants' mobile phones to track green spaces (through photos) and emotional wellbeing through questionnaires

* [**Characterizing Exposomes: Tools for Measuring Personal Environmental Exposures**](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3339478/)
  A review of the tools and database management practices used to explore internal and external exposome - as intended in the narrow definition

* [**The nature of nurture: refining the definition of the exposome**](https://www.ncbi.nlm.nih.gov/pubmed/24213143)
  This articles provides a new, broader definition of exposome, one that take into consideration the cumulative measure of environmental influences and associated biological responses throughout the lifespan, including exposures from the environment, diet, behavior, and endogenous processes. Ultimately, health status is influenced by the interaction of an individual’s environmental and genetic factors from conception to death.

* [**The Next Big Thing in Health is Your Exposome**](https://medium.com/s/thenewnew/the-exposome-is-the-new-frontier-e5bb8b1360da)
  Interview to Stanford's professor Michael Snyder, a researcher of the exposome.

* [**The framework of urban exposome: Application of the exposome concept in urban health studies**](https://www.ncbi.nlm.nih.gov/pubmed/29729514)
  Researchers from Limassol experimented within the city the concept of urban exposome, meaning the continuous spatiotemporal monitoring of indicators associated with the urban external and internal domains that shape up the quality of life and the health of urban populations.


* [Sequencing the Public Health Genome]({{site.baseurl}}/documents/Sequencing the Public Health Genome.pdf) and [The Public Health Exposome: A Population-Based, Exposure Science Approach to Health Disparities Research](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4276651/)
  These articles introduce the concept of public health exposome, suggesting a framework and a methodology to investigate health disparities at a population level.

+ [Assessing the Exposome with External Measures: Commentary on the State of the Science and Research Recommendations]({{site.baseurl}}/documents/Assessing the Exposome with External Measures- Commentary on the State of the Science and Research Recommendations.pdf)
  This article provide a summary and a bibliography on the main studies carried in the field of public exposome.

* [Exposure Science, the Exposome, and Public Health]({{site.baseurl}}/documents/Exposure Science, the Exposome, and Public Health.pdf)
  This article argues in favor of a broad approach to the exposome, moving from an individual, internal perspective to a holistic approach to ecosystem health. It introduce the concept of eco-exposome

* [Exposure Science in the 21st Century: A Vision and a Strategy](https://www.nap.edu/read/13507/chapter/5)
   2012 National Academy of Science's strategic paper proposing eco-exposome as a new holistic framework to evaluate exposures

* [The social exposome](https://www.youtube.com/watch?v=iQCGPqZ_rx8)
  This video introduces the concept of social exposome, which takes into account social interactions and behaviors as influences on the exposome: social stressors and relationships can act like environmental toxins.

* [The socio-exposome: advancing exposure science and environmental justice in a postgenomic era]({{site.baseurl}}/documents/The socio-exposome advancing exposure science and environmental justice in a postgenomic era.pdf)
  The exposome from a sociological point of view: which are the factors driving our exposure to hazards? Social and economical inequalities have to be taken into consideration as a factor influencing our individual and collective exposome


**Projects on exposome**

* [The Human Exposome Project: a toolbox for assessing and addressing the impact of environment on health](https://www.efsa.europa.eu/en/funding/calls/human-exposome-project-toolbox-assessing-and-addressing-impact-environment-health)
  H2020 currently active EU project to fund biomedical research on the Exposome[Exposome and

* [Exposomics](https://www.cdc.gov/niosh/topics/exposome/default.html)

* [EXPOsOMICS](https://www.isglobal.org/en/-/exposomics)



**Tools to explore the exposome**

* [Social sensing for epidemiological behavior change]({{site.baseurl}}/documents/Social sensing for epidemiological behavior change.pdf)
  This paper proposes a mobile phone based co-location and communication sensing to measure characteristic behavior changes in a population

* [Use of Deep Learning to Examine the Association of the Built Environment With Prevalence of Neighborhood Adult Obesity](https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2698635)
  Researchers used deep learning to map obesity across 8 U.S. counties. Studies have shown that certain features of the built environment can be associated with obesity and physical activity across different life stages. Without using medical data - only analyzing urbvan features - the machine guessed almost exactly the obesity rate.

* [Exposome explorer](http://exposome-explorer.iarc.fr/)
  Database dedicated to biomarkers of exposure to environmental risk factors for diseases


**Tools to expolore the social layer of the exposome**

* [Are you getting sick? Predicting influenza-like symptoms using human mobility behaviors](https://epjdatascience.springeropen.com/articles/10.1140/epjds/s13688-017-0124-6)
  Mobility is a key factor in transmittin illnesses. By predicting infections through mobility patterns, this paper gives a useful framework to predict the appearence of illnesses

* [The Death and Life of Great Italian Cities:A Mobile Phone Data Perspective](https://www.researchgate.net/publication/298721861_The_Death_and_Life_of_Great_Italian_Cities_A_Mobile_Phone_Data_Perspective)
  This paper aims to assess metrics for the urban fabric (as defined by J. Jacobs in The Death and Life of Great American Cities) to assess the level of urban vitality by using mobile phone data and data gathered by the Italian Census. The paper offers interesting metrics to play with to assess land use, small blocks, aged buildings, concentration and vacuums.

* [Sensing, Understanding, and Shaping Social Behavior](https://ieeexplore.ieee.org/document/6804686)
  This 2014 paper offers a review of practices to sense understand and shape human behaviors by using passive sensors.

* [The Community Wellbeing Framework](http://www.dialogdesign.ca/community-wellbeing/)
  Canadian project aimed at designing for communities' wellbeing


## Relevant weak signals

**Climate change**

* [The effects of climate change on human health – and what to do about them](http://scopeblog.stanford.edu/2017/05/09/the-effects-of-climate-change-on-human-health-and-what-to-do-about-them/)

* [Why we should care that climate change is making us sick](https://scopeblog.stanford.edu/2017/12/20/why-we-should-care-that-climate-change-is-making-us-sick/)

**AI and doctors**

* [Why AI will make healthcare personal](https://www.weforum.org/agenda/2019/03/why-ai-will-make-healthcare-personal/)
Assessing the impacts of AI in Healthcare: personalized medicine will spread, costs will be reduced

* [How Artificial Intelligence Can Make Doctors More Human](https://onezero.medium.com/how-artificial-intelligence-can-make-doctors-more-human-9424f5a8122e)

* [Your Future Doctor May Not be Human. This Is the Rise of AI in Medicine](https://futurism.com/ai-medicine-doctor)

* [Confronting Dr Robot Creating a people powered future for AI in health ](https://media.nesta.org.uk/documents/confronting_dr_robot.pdf)

* [AI can’t replace doctors. But it can make them better](https://www.technologyreview.com/s/612277/ai-cant-replace-doctors-but-it-can-make-them-better/)

* [Meet the GP of the future](https://www.telegraph.co.uk/wellbeing/future-health/the-doctor-of-the-future/)

* [Rethinking health innovation](https://www.ucl.ac.uk/bartlett/public-purpose/research/research-streams/rethinking-health-innovation)

* [Digital medicine: bad for our health?](https://www.ft.com/content/3ed1cc6c-0612-11e8-9650-9c0ad2d7c5b5)

**Patterns of urbanization**

* [Is Inclusive smarter than Smart?](https://medium.com/@UNDPEurasia/are-inclusive-cities-smarter-than-smart-13e2db4c407f)

* [Smart Cities: Ordinary Citizens Were The Missing Link All Along](https://towardsdatascience.com/smart-cities-ordinary-citizens-were-the-missing-link-all-along-7487e1753e10)

* [Barcelona Lab for Urban Environmental Justice and Sustainability](http://www.bcnuej.org/)

* [The urban’: a concept under stress in an interconnected world](http://theconversation.com/the-urban-a-concept-under-stress-in-an-interconnected-world-61000)

**Data as a tool for change**

* [Urban Data Pionieers](https://www.cityoftulsa.org/government/performance-strategy-and-innovation/urban-data-pioneers/)

* [IRYO](https://iryo.io/#intro)

* [Decibel.Live](https://www.decibel.live/)

* [Let’s make private data into a public good](https://www.technologyreview.com/s/611489/lets-make-private-data-into-a-public-good/)

* [ALGORITHMIC JUSTICE LEAGUE](https://www.ajlunited.org/)

* [Caring through Data: Attending to the Social and Emotional Experiences of Health Dataification](https://static1.squarespace.com/static/52842f7de4b0b6141ccb7766/t/5873a01137c581ab80198711/1483972626721/Kaziunas+et+al.+2017+Caring+Through+Data.pdf)

* [Self-Tracking for Health and the Quantified Self: Re-Articulating Autonomy, Solidarity, and Authenticity in an Age of Personalized Healthcare](https://www.researchgate.net/publication/301481639_Self-Tracking_for_Health_and_the_Quantified_Self_Re-Articulating_Autonomy_Solidarity_and_Authenticity_in_an_Age_of_Personalized_Healthcare)

* [When digital health meets digital capitalism, how many common goods are at stake?](https://www.researchgate.net/publication/329804081_When_digital_health_meets_digital_capitalism_how_many_common_goods_are_at_stake)

* [Harnessing the Power of Data in Health](https://med.stanford.edu/content/dam/sm/sm-news/documents/StanfordMedicineHealthTrendsWhitePaper2017.pdf)


**Urban wellbeing**

* [Cities and Mental Health](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5374256/)

* [Urban Emotions—Geo-Semantic Emotion Extraction from Technical Sensors, Human Sensors and Crowdsourced Data](https://www.researchgate.net/publication/268630662_Urban_Emotions-Geo-Semantic_Emotion_Extraction_from_Technical_Sensors_Human_Sensors_and_Crowdsourced_Data)

* [Health in Public Spaces: The challenge of inactive citizens for cities](https://urbact.eu/health-public-spaces-challenge-inactive-citizens-cities)

* [ECDC country visit to Italy to discuss antimicrobial resistance issues](https://ecdc.europa.eu/sites/portal/files/documents/AMR-country-visit-Italy.pdf)

* [City living marks the brain](https://www.nature.com/news/2011/110622/full/474429a.html)

* [MetaSUB: Metagenomics & Metadesign of Subways & Urban Biomes](http://metasub.org/)


## Determinants of health & wellbeing in urban context

* [A Conceptual Framework for Action on the Social Determinants of Health](https://www.who.int/sdhconference/resources/ConceptualframeworkforactiononSDH_eng.pdf)

* [Urban as a Determinant of Health](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1891649/)

* [Science and the Future of Cities](https://www.nature.com/documents/Science_and_the_future_of_cites.pdf)

* [WHO - The determinants of health](https://www.who.int/hia/evidence/doh/en/index2.html)

* [Urban Health & Wellbeing](https://council.science/what-we-do/research-programmes/thematic-organizations/urban-health-wellbeing)

* [Health in cities: is a systems approach needed?](http://www.scielo.br/scielo.php?script=sci_arttext&pid=S0102-311X2015001300009)

* [Environments should improve not harm our health](https://www.who.int/mediacentre/commentaries/environments-should-improve-our-health/en/)

* [Preventing disease through healthy environments: a global assessment of the burden of disease from environmental risks](https://www.who.int/quantifying_ehimpacts/publications/preventing-disease/en/)

* [**10 facts on preventing disease through healthy environments**](https://www.who.int/features/factfiles/environmental-disease-burden/en/)

* [Urban Planning, Environment and Health](https://www.isglobal.org/en/urban-planning)

* [Social determinants of health](http://www.euro.who.int/en/health-topics/health-determinants/social-determinants/social-determinants)

* [**Promoting a healthy cities agenda through indicators: development of a global urban environment and health index**](https://www.tandfonline.com/doi/full/10.1080/23748834.2018.1429180)

## Resources for intervention

* [How to categorize health data](https://tincture.io/a-taxonomy-of-health-data-cf11bb26fc0e)

* [What is wellbeing and can we design for it?](https://medium.com/benefit-mindset/what-is-wellbeing-and-can-we-design-for-it-4bd69cd95142)

## Systemic thinking for interventions

* [Societal Innovation & Our Future Cities - The Case for a Social Contract for Innovation](https://provocations.darkmatterlabs.org/the-societal-contract-for-innovation-15593ae9a1d4)

* [The Web of Life: A New Scientific Understanding of Living Systems](https://www.amazon.es/Web-Life-Scientific-Understanding-Systems/dp/0385476760)

* [How to Design Social Systems (Without Causing Depression and War)](https://medium.com/what-to-build/how-to-design-social-systems-without-causing-depression-and-war-3c3f8e0226d1)

* [Extended intelligence](https://pubpub.ito.com/pub/extended-intelligence)

* [ The ID question](https://howwegettonext.com/the-id-question-6fb3b56052b5)

* [Designing for the In-Between](https://medium.com/s/world-wide-wtf/designing-for-the-in-between-hybrids-1990s-net-art-and-a-giant-floating-worm-34be64b872d3)

* [Leverage Points: Places to Intervene in a System](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)

* [Stranger Studies 101: Cities as Interaction Machines](https://www.theatlantic.com/technology/archive/2010/09/stranger-studies-101-cities-as-interaction-machines/62315/)

* [Design and intervention in the age of no alternative](https://static1.squarespace.com/static/52842f7de4b0b6141ccb7766/t/5c33b36f8a922d30924786cb/1546892145182/cscw109-lindtner.pdf)

* [How methods make designers](https://static1.squarespace.com/static/52842f7de4b0b6141ccb7766/t/58739f67d1758e4368e2da13/1483972456076/How+Methods+Make+Designers+-+Camera+ready+-+01.06.17-+final.pdf)

## Wellbeing indicators
* [World Happiness Report 2018](http://worldhappiness.report/)

* [OECD Regional Wellbeing](https://www.oecdregionalwellbeing.org/)
* [ISTAT Bes](https://www.istat.it/it/benessere-e-sostenibilit%C3%A0/la-misurazione-del-benessere-(bes)/gli-indicatori-del-bes)

* [Gross National Happiness](https://www.grossnationalhappiness.com/)

## Urban health and living experiments (mostly sensing)
* [CareAi — Mixing Healthcare Data with Emergent Technologies for Impact](https://medium.com/@lucaslorenzop/careai-ideasforchange-26d2baf9e757)

* [Making Sense Toolkit](http://making-sense.eu/publication_categories/toolkit/)

* [Citizen Science for public health](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6005099/)

* [Smart and Sustainable Built Environment Experimentation at scale: challenges for making urban informatics work](https://www.dropbox.com/s/nombrpb1o5uw696/SASBE-10-2017-0054.pdf?dl=0)

* [A City in Common: A Framework to Orchestrate Large-scale Citizen Engagement aroundUrban Issues](https://www.dropbox.com/s/y2ve47sy0nfk12p/CHI%202017_cameraready_final.pdf?dl=0)

* [The Quantified Community and Neighborhood Labs: A Framework for Computational Urban Science and Civic Technology Innovation](https://www.tandfonline.com/doi/full/10.1080/10630732.2016.1177260)

* [IID-2018 - Session 3 - Urban governance](https://www.youtube.com/watch?time_continue=2&v=RFFXMPT2qfA)

* [NYC Pollnscape](https://urbanintelligencelab.github.io/NYC_PollenScape/)

* [Urban intelligence Lab](http://www.urbanintelligencelab.org/)
[Sense2Health: A Quantified Self Application for Monitoring Personal Exposure to Environmental Pollution](https://hal.inria.fr/hal-01102275/)

* [Photos on social media can predict the health of neighborhoods](https://techcrunch.com/2018/06/06/photos-on-social-media-can-predict-the-health-of-neighborhoods/)

* [Sidewalk Labs ](https://www.sidewalklabs.com/)

* [Invisible dust](http://invisibledust.com/)

* [Reality mining: sensing complex social systems](https://link.springer.com/article/10.1007/s00779-005-0046-3)

* [Germ City Microbes and the Metropolis](https://www.mcny.org/exhibition/germ-city)


## How to make data interaction meaningful

* [Dear Data](http://www.dear-data.com/)

* [Can software be good for us](https://medium.com/what-to-build/dear-zuck-fd25ecb1aa5a)

* [Salus.Coop - Citizen cooperative of health data](https://www.saluscoop.org/)
