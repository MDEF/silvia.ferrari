---
layout: page
title: Cognitive systems in AI
permalink: /china/
---

<iframe src="https://player.vimeo.com/video/312961246" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/312961246">Shenzhen research trip - MDEF Ai group.</a> from <a href="https://vimeo.com/user92093114">G&aacute;bor L&aacute;szlo M&aacute;ndoki</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

Our research into AI in China revolved around three keywords, three themes that recurred in our observations. We believe future developments in the field of AI will be very much influenceced by:

* Infrastructure: the ability to develop new ideas into tangible things with flexibility and speed of execution

* Data & trust : a seemingly general attitude towards data sharing that values trust more than individual protection

* Innovation: a mindset that prioritize improving existing ideas more than disrupting the system


<div data-configid="35847251/68450372" style="width:640px; height:360px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>


Our individual reflections on the trip:

[Oliver Juggins](https://mdef.gitlab.io/oliver.juggins/masterproject/china/)

[Silvia Ferrari](https://mdef.gitlab.io/silvia.ferrari/two/on-china/)

[Gabor Mandoki](https://mdef.gitlab.io/gabor.mandoki/designstudio/term2/2019/01/28/Shenzhen-research-trip/)

[Maite Villar](https://mdef.gitlab.io/maite.villar/reflections/Shenzhen/)
